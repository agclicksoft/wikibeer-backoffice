<!DOCTYPE html>
<html lang="pt-br">
<head class="teste funcionando">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi" />
    <link rel="icon" href="<?php echo $this->Url->build('/img/favicon.png'); ?>" type="image/gif" sizes="16x16">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= ($title != '' && $title != null) ? $title : "Login" ?></title>
    <title><?= $this->fetch('title') ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <?= $this->fetch('meta') ?>
    <?=  $this->Html->css('custom'); ?>
    <?=  $this->Html->css('login'); ?>
    <?= $this->fetch('css') ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

    <script type="text/javascript">
        //esconde o footer quando o keyboard aparecer
        document.write( '<style>#footer{visibility:hidden}@media(min-height:' + (window.innerWidth - 10) + 'px){#footer{visibility:visible}}</style>' );
    </script>

<body>
      <?php
          if ($isLogged)
          {
                $default_nav_bar = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'nav-bar.ctp';
                if (file_exists($default_nav_bar)) {
                    ob_start();
                    include $default_nav_bar;
                    echo ob_get_clean();
                }
                else
                {
                    // echo $this->element('nav-bar');
                }

            }
        ?>

    <section class="container clearfix" id"sect-container">

             <?= $this->Flash->render() ?>
        <?= $this->Flash->render('auth') ?>

        <?php
            if ($isLogged)
            {
                $default_left_bar = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'left-bar.ctp';
                if (file_exists($default_left_bar))
                {
                    ob_start();
                    include $default_left_bar;
                    echo ob_get_clean();
                }
                else
                {
                    // echo $this->element('left-bar');
                }

            }
        ?>

        <?= $this->fetch('content') ?>
    </section>

    <?php
        $default_footer = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'footer.ctp';
        if (file_exists($default_footer)) {
            ob_start();
            include $default_footer;
            echo ob_get_clean();
        }
        else {
            echo $this->element('footer');
        }
    ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
    <?php
        echo $this->Html->script('jquery.min.js');
        echo $this->Html->script('bootstrap.min.js');
        echo $this->Html->script('login');
        // echo $this->Html->script('jquery.maskedinput.min');
    ?>

    <?= $this->fetch('script'); ?>

</body>
</html>

