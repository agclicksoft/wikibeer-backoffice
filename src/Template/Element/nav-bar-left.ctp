<ul class="nav navbar-nav">
    <li><a href="<?php echo $this->Url->build(['controller' => 'Beers', 'action' => 'index']); ?>">Cervejas</a></li>
    <li><a href="<?php echo $this->Url->build(['controller' => 'Suggestions', 'action' => 'index']); ?>">Sugestões</a></li>
    <li><a href="<?php echo $this->Url->build(['controller' => 'Breweries', 'action' => 'index']); ?>">Cervejarias</a></li>
    <li><a href="<?php echo $this->Url->build(['controller' => 'Breweries', 'action' => 'index', '?' => ['type' => 'bars']]); ?>">Bares</a></li>
    <li><a href="<?php echo $this->Url->build(['controller' => 'News', 'action' => 'index']); ?>">Notícias</a></li>
    <li><a href="<?php echo $this->Url->build(['controller' => 'News', 'action' => 'cult']); ?>">Cult</a></li>
</ul>
