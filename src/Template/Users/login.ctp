<!--    <div class="users form">
   <?= $this->Flash->render('auth') ?>
   <?= $this->Form->create() ?>
       <fieldset>
           <legend><?= __('Por favor enforme o seu email e senha') ?></legend>
           <?= $this->Form->input('email') ?>
           <?= $this->Form->input('password') ?>
       </fieldset>
   <?= $this->Form->button(__('Entrar')); ?>
   <?= $this->Form->end() ?>
   </div> -->
<div class=" vertical-center">
      <div class="row align-middle">
         <div class="container-center">


         <div class="col-lg-12 col-md-12 col-sm-12 ">

         <div class="logo-container col-lg-6 col-md-6 col-sm-6" >
            <div>
               <?php
                  echo $this->Html->image('logo_beer.png', ['alt' => 'Beer', 'width'=>'80%']);
                ?>
            </div>
         </div>

          <div class="login-container col-lg-4 col-md-4 col-sm-4" >

            <?= $this->Flash->render('auth') ?>
            <!-- <h4 class="text-center" style="border-bottom: 1px solid #c5c5c5;"> -->
            <div id="form-olvidado">
               <!-- <form accept-charset="UTF-8" role="form" id="login-form" method="post"> -->
               <?= $this->Form->create() ?>
               <h5 class="">
                  Email
               </h5>
               <fieldset>
                  <div class="form-group input-group login-input">
                     <span class="input-group-addon">
                     @
                     </span>
                     <!-- <input class="form-control" placeholder="Email" name="email" type="email" required="" autofocus=""> -->
                     <input class="form-control" placeholder="Email" type="text" name="email" id="email" autofocus="" required="">
                  </div>
                  <h5 class="">
                     Senha
                  </h5>
                  <div class="form-group input-group login-input">
                     <span class="input-group-addon">
                     <i class="glyphicon glyphicon-lock">
                     </i>
                     </span>
                     <!-- <input class="form-control" placeholder="Password" name="password" type="password" value="" required=""> -->
                     <input type="password" placeholder="Senha"  name="password" id="password" class="form-control" required="">
                  </div>
                  <div class="form-group">
                     <button type="submit" class="btn btn-primary btn-block">
                     Entrar
                     </button>
                     <!-- <p class="help-block">
                        <a class="pull-right text-muted" href="#" id="olvidado"><small>Esqueceu sua senha?</small></a>
                     </p> -->
                  </div>
               </fieldset>
               <!-- </form> -->
               <?= $this->Form->end() ?>
            </div>
            <div style="display: none;" id="form-olvidado1">
               <h4 class="">
                  Esqueceu sua senha?
               </h4>
               <!-- <form accept-charset="UTF-8" role="form" id="login-recordar" method="post"> -->
               <?=  $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'recuperateEmail'] ]);?>
               <fieldset>
                  <span class="help-block">
                  Digite o endereço de email que você usa em sua conta.
                  <br>
                  Você recebera um email com instruções para recuperar sua senha.
                  </span>
                  <div class="form-group input-group login-input">
                     <span class="input-group-addon">
                     @
                     </span>
                     <input class="form-control" placeholder="Email" name="email" type="email" required="">
                  </div>
                  <button type="submit" class="btn btn-primary btn-block" id="btn-olvidado">
                  Continuar
                  </button>
                  <p class="help-block">
                     <a class="text-muted" href="#" id="acceso1"><small>Voltar ao login</small></a>
                  </p>
               </fieldset>
               <!-- </form> -->
               <?= $this->Form->end() ?>
            </div>
         </div>
         </div>
      </div>
   </div>
</div>
