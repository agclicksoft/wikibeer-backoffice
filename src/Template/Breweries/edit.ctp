<div class="beers form col-md-10 columns content">
    <br>
    <h1 class="page-title"><?=$brewery->type == 0 ? 'Editar Cervejaria/Microcervejaria':'Editar Bar/Pub'?></h1>
    <br>
    <p class="red">Os campos com * são obrigatórios</p>
    <br>
    <?=$this->Form->create($brewery, ['type' => 'file'])?>
    <?php
$optImg = [
    'link' => 'Usar um link externo',
    'upload' => 'Fazer upload da imagem',
];
?>
    <fieldset>
        <div class="row">
            <div class="col-md-8">
                <?=$this->Form->input('name', ['required' => 'required', 'label' => ['text' => 'Nome:', 'class' => 'mandatory']]);?>
            </div>
            <?php /*<div class="col-md-4">
                <div class="form-group required">
                    <label class="mandatory">Tipo:</label>
                    <?=$this->Form->select('type', ['Cervejaria', 'Microcervejaria', 'Bar', 'Pub'], ['empty' => 'Escolha', 'class' => 'form-control'])?>
                </div>
            </div>*/?>
        </div>

        <div class="row">
            <div class="col-md-2">
                <?=$this->Form->input('zipcode', ['required' => 'required', 'class' => 'decimal', 'type' => 'text', 'label' => ['class' => 'mandatory', 'text' => 'CEP:']]);?>
            </div>
            <div class="col-md-5">
                <?=$this->Form->input('street', ['required' => 'required', 'class' => '', 'type' => 'text', 'label' => ['class' => 'mandatory', 'text' => 'Endereço:']]);?>
            </div>
            <div class="col-md-5">
                <?=$this->Form->input('complement', ['class' => '', 'type' => 'text', 'label' => ['text' => 'Complemento:']]);?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?=$this->Form->input('city', ['required' => 'required', 'class' => '', 'type' => 'text', 'label' => ['class' => 'mandatory', 'text' => 'Cidade:']]);?>
            </div>
            <div class="col-md-4">
                <?=$this->Form->input('state', ['required' => 'required', 'class' => '', 'type' => 'text', 'label' => ['class' => 'mandatory', 'text' => 'Estado:']]);?>
            </div>
            <div class="col-md-4">
                <?=$this->Form->input('country', ['required' => 'required', 'class' => '', 'type' => 'text', 'label' => ['class' => 'mandatory', 'text' => 'País:']]);?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <?=$this->Form->input('website', ['class' => '', 'type' => 'text', 'label' => ['text' => 'Site:']]);?>
            </div>
            <div class="col-md-4">
                <?=$this->Form->input('email', ['class' => '', 'type' => 'email', 'label' => ['text' => 'Email:']]);?>
            </div>
          <div class="col-md-3">
                <?=$this->Form->input('telephone', ['id' => 'tel',
                'onkeypress'=> "mask(this, mphone);", 'onblur'=> "mask(this, mphone);",
                'class' => '', 'type' => 'text', 'label' => ['text' => 'Telefone:']]);?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?=$this->Form->input('facebook', ['class' => '', 'type' => 'text', 'label' => ['text' => 'Facebook:']]);?>
            </div>
            <div class="col-md-4">
                <?=$this->Form->input('instagram', ['class' => '', 'type' => 'text', 'label' => ['text' => 'Instagram:']]);?>
            </div>
            <!--<div class="col-md-4">
                <?=$this->Form->input('image_link', ['class' => '', 'type' => 'text', 'label' => ['text' => 'Link da Imagem:']]);?>
            </div>-->
        </div>
        <div class="row">
          <div class="col-md-8">
              <label class="mandatory" for="state">Descrição:</label>
              <?=$this->Form->textarea('description', ['class' => '', 'required' => 'required', 'type' => 'text', 'label' => false]);?>
          </div>
          <div class="col-md-4">
              <div class="row">
                  <div class="col-md-12">
                      <?=$this->Form->hidden('new_image_link', ['id' => 'new-image-link', 'value' => ''])?>
                      <?=$this->Form->hidden('image_changed', ['id' => 'image-changed', 'value' => '0'])?>
                      <button type="button" id="nova-imagem" class="btn btn-default">Nova imagem</button>
                      <input type="file" id="nova-imagem-input" style="display: none;">
                  </div>
                  <div class="col-md-12">
                    <img id="image-preview" src="<?=$brewery->getImageUrl()?>" width="100%">
                  </div>
              </div>
          </div>
        </div>
    </fieldset>

    <button type="button" class="btn btn-default red-buttons" onclick="goBack()">Voltar</button>
    <button type="submit" class="btn btn-default yellow-buttons">Salvar</button>
    <?=$this->Form->end()?>
    <br><br>
</div>

<script>
$(document).ready(function () {
    $('#nova-imagem').click(function (event) {
        event.preventDefault();
        $('#nova-imagem-input').click();
    });

    $("#nova-imagem-input").change(function() {
        const input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image-preview').attr('src', e.target.result);
                $('#image-changed').val('1');
                $('#new-image-link').val(e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
});

function mask(o, f) {
  setTimeout(function() {
    var v = mphone(o.value);
    if (v != o.value) {
      o.value = v;
    }
  }, 1);
}

function mphone(v) {
  var r = v.replace(/\D/g, "");
  r = r.replace(/^0/, "");
  if (r.length > 12) {
    r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
  } else if (r.length > 10) {
    r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
  } else if (r.length > 5) {
    r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
  } else if (r.length > 2) {
    r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
  } else {
    r = r.replace(/^(\d*)/, "($1");
  }
  return r;
}
</script>
