
<div class="row">

<div class="beers index col-md-12 columns content">

    <div class="buttons">
    <?php
      if ($type == 'bars') {
        echo $this->Html->link(__('Cadastrar Bar'), ['action' => 'add', '?' => ['type' => 'bars']], ['class'=>'btn yellow-buttons new-beer btn-default']);
      } else {
        echo $this->Html->link(__('Cadastrar Cervejaria'), ['action' => 'add'], ['class'=>'btn yellow-buttons new-beer btn-default']);
      }
    ?>
        <!--<button class="btn yellow-buttons btn-default import-button" data-toggle="modal" data-target="#myModal">Importar</button>-->
    </div>

    <?= $this->Form->create('index', ['type'=>'get','class'=>'search-form']) ?>
  
      <div class="form-group">
       
       <input type="text" id="campo-busca" class="form-control inline-field small-field" placeholder="Digite aqui sua busca" name="busca">
    <?php if (isset($_GET['type']) && $_GET['type'] == 'bars') : ?>
       <input type="hidden" name="type" value="bars" />

    <?php endif;?>
       <?= $this->Form->button(__('Filtrar',true))?>
      
      </div>

    <?= $this->Form->end() ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th class="left-align"><?= $this->Paginator->sort('name','Nome',['direction' => 'desc']) ?></th>
                <th class="right-align"><?= $this->Paginator->sort('type','Tipo',['direction' => 'desc']) ?></th>
                <th class="right-align"><?= $this->Paginator->sort('city','Cidade',['direction' => 'desc']) ?></th>
                <th class="right-align"><?= $this->Paginator->sort('state','Estado',['direction' => 'desc']) ?></th>
                <th class="actions right-align"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($breweries as $brewery): 

                $edit_img   =  $this->Html->image('edit-icon.gif', ['class' => 'action-icons']); 
                $delete_img =  $this->Html->image('delete-icon.png', ['class' => 'action-icons']);
            ?>
            <tr>
                <td class="left-align"><?=$brewery->name?></td>
                <td class="right-align"><?=$brewery->getTypeName()?></td>
                <td class="right-align"><?=$brewery->city?></td>
                <td class="right-align"><?=$brewery->state?></td>
                <td class="actions right-align" style="white-space:nowrap">
                    <?= $this->Html->link($edit_img, ['action' => 'edit', $brewery->id], ['escape'=>false]) ?>
                    <?= $this->Form->postLink($delete_img, ['action' => 'deactivate', $brewery->id],['escape'=>false]) ?>
                </td>
                <?php /*<!-- <td><?= $this->Number->format($beer->id) ?></td> -->
                <td class="left-align"><?= h($beer->beer_name) ?></td>
                <!-- <td><?= h($beer->manufacturer_name) ?></td>
                <td><?= $this->Number->format($beer->alcohol_content) ?></td>
                <td><?= h($beer->bitterness) ?></td>
                <td><?= h($beer->harmonization) ?></td>
                <td><?= h($beer->style) ?></td> -->
                <td class="right-align"><?= h($beer->visits) ?></td>
                <td class="right-align"><?= h($beer->evaluations_total) ?></td>
                <td class="actions right-align" style="white-space:nowrap">
                   
                    <?= $this->Html->link($edit_img, ['action' => 'edit', $beer->id], ['escape'=>false]) ?>

                    <?= $this->Form->postLink($delete_img, ['action' => 'deactivate', $beer->id],['escape'=>false]) ?>

                </td>*/ ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <center>
        <div class="paginator">

                <ul class="pagination">
                    <?= $this->Paginator->prev('&laquo; ' . __('anterior'), ['escape'=>false]) ?>
                    <?= $this->Paginator->numbers(['escape'=>false]) ?>
                    <?= $this->Paginator->next(__('próximo') . ' &raquo;', ['escape'=>false]) ?>
                </ul>
                <!-- <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
             {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p> -->
        </div>
    </center>
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Importar Cervejas</h4>
      </div>
      
      <div style="padding: 40px;">
          <?= $this->Form->create('index', ['id'=>'import-form','type'=>'file','url'=>'/beers/import']) ?>
            <?= $this->Form->input('csv',['required'=>'required','type'=>'file','label'=>['text'=>'Selecione o arquivo para importação:','id'=>'csv-input']]); ?>
            <button id="subButton" type="submit" onclick="this.form.submit()"  class="btn btn-default yellow-buttons">Iniciar importação</button>
            <br>
            <div class="hide-loader" style="display: none">
                 <div class="loader hide-loader" ></div><span>Importando, por favor aguarde...</span>
            </div>
           
          <?= $this->Form->end() ?>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn close-modal btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    $('document').ready(function(){
        
        $('.close-modal').click(function(){
            $('#import-form')[0].reset();
            $('.hide-loader').hide();
            $('#subButton').show();
            $('#subButton').attr('disabled',false);
        });

        $('#subButton').click(function(){
            $(this).attr('disabled',true);
            $(this).hide();
            $('.hide-loader').show();
        });
        
    });
    
    

</script>