<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('List {0}', 'Suggestions'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="suggestions form col-md-10 columns content">
    <?= $this->Form->create($suggestion) ?>
    <fieldset>
        <legend><?= 'Add Suggestion' ?></legend>
        <?php
            echo $this->Form->input('beer_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
