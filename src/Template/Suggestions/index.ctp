<div class="row">

<div class="suggestions index col-md-10 columns content">
     <h1 class="page-title"><?= 'Sugestões dos usuários' ?></h1>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th class="left-align"><?= $this->Paginator->sort('beer_name','Sugestões') ?></th>
                <th class="right-align"><?= $this->Paginator->sort('suggestions_total','Número de sugestões',['direction' => 'desc']) ?></th>
                <th class="actions right-align"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($suggestions as $suggestion): 
                $delete_img =  $this->Html->image('delete-icon.png', ['class' => 'action-icons']);
            ?>
                
            <tr>
               <td class="left-align"><?= h($suggestion->beer_name) ?></td>
               <td class="right-align"><?= $this->Number->format($suggestion->suggestions_total) ?></td>
               <td class="actions right-align" style="white-space:nowrap">
                 <?= $this->Form->postLink($delete_img, ['action' => 'delete', $suggestion->id],['escape'=>false,'confirm' => 'Tem certeza que deseja deletar?']) ?>
               </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <center>
        <div class="paginator">
        
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('anterior'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('próxima') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <!-- <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p> -->
        </div>
    </center>
</div>
</div>