<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Suggestion']), ['action' => 'edit', $suggestion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Suggestion']), ['action' => 'delete', $suggestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suggestion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Suggestions']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Suggestion']), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="suggestions view col-lg-10 col-md-9">
    <h3><?= h($suggestion->id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Beer Name</th>
            <td><?= h($suggestion->beer_name) ?></td>
        </tr>
        <tr>
            <th>'Id</th>
            <td><?= $this->Number->format($suggestion->id) ?></td>
        </tr>
        <tr>
            <th>'Sugestions Total</th>
            <td><?= $this->Number->format($suggestion->sugestions_total) ?></td>
        </tr>
        <tr>
            <th>Created</th>
            <td><?= h($suggestion->created) ?></tr>
        </tr>
    </table>
</div>
