<div class="beers form col-md-10 columns content">
    <br>
    <h1 class="page-title"><?= 'CADASTRAR CERVEJA' ?></h1>
    <br>
    <p class="red">Os campos com * são obrigatórios</p>
    <br>
    <?= $this->Form->create($beer,['type' => 'file']) ?>
    <?php
        $optImg=[
        'link'=>'Usar um link externo',
        'upload'=>'Fazer upload da imagem'
        ];

    ?>
    <fieldset>
        <div class="row">
            <div class="col-md-5">
                <?= $this->Form->input('beer_name',['required'=>'required','label'=>['text'=>'Nome da Cerveja:','class'=>'mandatory']]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 inline-flex-add">

                    <?= $this->Form->input('barcodes.0.barcode',['label'=>['text'=>'Código de Barras:']]); ?>

                    <i onclick="addField()" class="glyphicon glyphicon-plus icon-add-remove-fields" aria-hidden="true"></i>

            </div>

            <div class="col-md-6">
                <?= $this->Form->input('manufacturer_name',['label'=>['text'=>'Cervejaria:']]); ?>
            </div>

        </div>

        <div class="row barcode-fields">
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('average_rating',['required'=>'required','class'=>'decimal mini-field','type'=>'text','label'=>['class'=>'mandatory','text'=>'Nota de avaliação:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('evaluations_total',['class'=>'mini-field','type'=>'text','label'=>['text'=>'Número de avaliadores:']]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('alcohol_content',['class'=>'decimal3 mini-field','type'=>'text','label'=>['text'=>'Àlcool (%ABU):']]); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('bitterness',['required'=>'required','class'=>'mini-field','label'=>['class'=>'mandatory','text'=>'Amargor (IBU):']]); ?>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('average_price',['class'=>'decimal3 mini-field','type'=>'text','label'=>['text'=>'Preço médio:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('packing',['class'=>'medium-field','label'=>['text'=>'Embalagem(ml):']]);?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6">
                 <?= $this->Form->input('country',['class'=>'medium-field','label'=>['text'=>'País:']]); ?>
            </div>
            <div class="col-md-6">
                 <?= $this->Form->input('state',['class'=>'medium-field','label'=>['text'=>'Estado:']]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('city',['class'=>'medium-field','label'=>['text'=>'Cidade:']]); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('colour',['class'=>'medium-field','label'=>['text'=>'Cor (visual):']]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('ideal_temperature',['class'=>' mini-field','label'=>['text'=>'Temperatura (C°):']]); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('calories',['class'=>'decimal3 mini-field','type'=>'text','label'=>['text'=>'Calorias (cal):']]); ?>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('glass_type',['class'=>'medium-field','label'=>['text'=>'Copo ideal:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('seasonality',['class'=>'medium-field','label'=>['text'=>'Sazonalidade:']]); ?>
            </div>

        </div>


        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('harmonization',['type'=>'textarea','label'=>['text'=>'Harmonização sugerida:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('comercial_presentation',['label'=>['text'=>'Descrição comercial:']]); ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('family',['class'=>'medium-field','label'=>['text'=>'Família:']]);?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('style',['class'=>'mini-field','label'=>['text'=>'Estilo:']]); ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('brewery_partner_group',['class'=>'medium-field','label'=>['text'=>'Parceria:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('importer',['class'=>'medium-field','label'=>['text'=>'Importador/Distribuidor:']]); ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('history',['class'=>'medium-field','label'=>['text'=>'Histórico:']]); ?>
            </div>

            <div class="col-md-6">
                <!-- <?= $this->Form->input('image_src_type',['class'=>'medium-field','options'=>$optImg,'type'=>'select','label'=>['text'=>'Tipo de imagem:']]); ?> -->

                <?= $this->Form->input('image',['class'=>'imageUpload','type'=>'file','label'=>['class'=>'imageUpload','text'=>'Imagem:']]); ?>

            </div>

        </div>
        <div class="row">

            <div class="col-md-6">



                <!-- <?= $this->Form->input('image_link',['class'=>'imageLink hidden-field','type'=>'text','label'=>['class'=>'imageLink hidden-field','text'=>'Link da Imagem:']]); ?> -->

                <?= $this->Form->input('lastImage',['type'=>'hidden']); ?>
            </div>
        </div>


    </fieldset>

    <button type="button" class="btn btn-default red-buttons" onclick="goBack()">Voltar</button>
    <button type="submit" class="btn btn-default yellow-buttons">Salvar</button>
    <?= $this->Form->end() ?>
    <br><br>
</div>

<script>

    // $("#image-src-type").change(function(){

    //     console.log($(this).val());
    //     if($(this).val() == 'link'){
    //         $('.imageLink').removeClass("hidden-field");
    //         $('.imageLink').attr('disabled',false);
    //         $('.imageUpload').addClass("hidden-field");
    //         $('.imageUpload').attr('disabled',true);
    //     }else{
    //         $('.imageUpload').removeClass("hidden-field");
    //         $('.imageUpload').attr('disabled',false);
    //         $('.imageLink').addClass("hidden-field");
    //         $('.imageLink').attr('disabled',true);
    //     }

    // });

    // if($("#image-src-type").val() == 'link'){
    //     $('.imageLink').removeClass("hidden-field");
    //     $('.imageLink').attr('disabled',false);
    //     $('.imageUpload').addClass("hidden-field");
    //     $('.imageUpload').attr('disabled',true);
    // }else{
    //     $('.imageUpload').removeClass("hidden-field");
    //     $('.imageUpload').attr('disabled',false);
    //     $('.imageLink').addClass("hidden-field");
    //     $('.imageLink').attr('disabled',true);
    // }



    jQuery(function($){
      $(".decimal").mask("9.99", {reverse: true});
       $(".decimal3").maskMoney({thousands:'', decimal:'.', allowZero:true});
    });

    var cont=1;
    var fieldNumber=cont+1;
    var html = '';

    function addField(){

      html =
            '<div class="fieldset-'+fieldNumber+' col-md-6 inline-flex-add ">'+
            '<div class="form-group text"><label for="barcode">Código de Barras '+fieldNumber+':</label>'+
            '<input type="text" name="barcodes['+cont+'][barcode]" id="barcode-'+cont+'" class="form-control"></div>'+
            '<i class="glyphicon glyphicon-minus icon-add-remove-fields" onclick="removeField('+fieldNumber+')"  aria-hidden="true"></i>'+
            '</div>';

      $('.barcode-fields').append(html);
      $('.barcode-fields').slideDown();
      cont++;
      fieldNumber++;

    }

    function removeField(number){
        cont--;
        fieldNumber--;
        $(".fieldset-"+number).remove();
    }


</script>
