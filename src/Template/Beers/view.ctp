<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Beer']), ['action' => 'edit', $beer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Beer']), ['action' => 'delete', $beer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $beer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Beers']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Beer']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Barcodes']), ['controller' => 'Barcodes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Barcode']), ['controller' => 'Barcodes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Ratings']), ['controller' => 'Ratings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Rating']), ['controller' => 'Ratings', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="beers view col-lg-10 col-md-9">
    <h3><?= h($beer->id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Beer Name</th>
            <td><?= h($beer->beer_name) ?></td>
        </tr>
        <tr>
            <th>Manufacturer Name</th>
            <td><?= h($beer->manufacturer_name) ?></td>
        </tr>
        <tr>
            <th>Bitterness</th>
            <td><?= h($beer->bitterness) ?></td>
        </tr>
        <tr>
            <th>Harmonization</th>
            <td><?= h($beer->harmonization) ?></td>
        </tr>
        <tr>
            <th>Style</th>
            <td><?= h($beer->style) ?></td>
        </tr>
        <tr>
            <th>Country</th>
            <td><?= h($beer->country) ?></td>
        </tr>
        <tr>
            <th>State</th>
            <td><?= h($beer->state) ?></td>
        </tr>
        <tr>
            <th>City</th>
            <td><?= h($beer->city) ?></td>
        </tr>
        <tr>
            <th>Colour</th>
            <td><?= h($beer->colour) ?></td>
        </tr>
        <tr>
            <th>Glass Type</th>
            <td><?= h($beer->glass_type) ?></td>
        </tr>
        <tr>
            <th>Image</th>
            <td><?= h($beer->image) ?></td>
        </tr>
        <tr>
            <th>'Id</th>
            <td><?= $this->Number->format($beer->id) ?></td>
        </tr>
        <tr>
            <th>'Alcohol Content</th>
            <td><?= $this->Number->format($beer->alcohol_content) ?></td>
        </tr>
        <tr>
            <th>'Calories</th>
            <td><?= $this->Number->format($beer->calories) ?></td>
        </tr>
        <tr>
            <th>'Ideal Temperature</th>
            <td><?= $this->Number->format($beer->ideal_temperature) ?></td>
        </tr>
        <tr>
            <th>'Average Price</th>
            <td><?= $this->Number->format($beer->average_price) ?></td>
        </tr>
        <tr>
            <th>'Status</th>
            <td><?= $this->Number->format($beer->status) ?></td>
        </tr>
        <tr>
            <th>Created</th>
            <td><?= h($beer->created) ?></tr>
        </tr>
        <tr>
            <th>Modified</th>
            <td><?= h($beer->modified) ?></tr>
        </tr>
    </table>
    <div class="row">
        <h4>Comercial Presentation</h4>
        <?= $this->Text->autoParagraph(h($beer->comercial_presentation)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related {0}', ['Barcodes']) ?></h4>
        <?php if (!empty($beer->barcodes)): ?>
        <table class="table table-striped table-hover">
            <tr>
                <th>Id</th>
                <th>Barcode</th>
                <th>Beer Id</th>
                <th>Created</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($beer->barcodes as $barcodes): ?>
            <tr>
                <td><?= h($barcodes->id) ?></td>
                <td><?= h($barcodes->barcode) ?></td>
                <td><?= h($barcodes->beer_id) ?></td>
                <td><?= h($barcodes->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Barcodes', 'action' => 'view', $barcodes->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Barcodes', 'action' => 'edit', $barcodes->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Barcodes', 'action' => 'delete', $barcodes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $barcodes->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related {0}', ['Ratings']) ?></h4>
        <?php if (!empty($beer->ratings)): ?>
        <table class="table table-striped table-hover">
            <tr>
                <th>Id</th>
                <th>Rating Score</th>
                <th>Beer Id</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($beer->ratings as $ratings): ?>
            <tr>
                <td><?= h($ratings->id) ?></td>
                <td><?= h($ratings->rating_score) ?></td>
                <td><?= h($ratings->beer_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Ratings', 'action' => 'view', $ratings->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Ratings', 'action' => 'edit', $ratings->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ratings', 'action' => 'delete', $ratings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ratings->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
