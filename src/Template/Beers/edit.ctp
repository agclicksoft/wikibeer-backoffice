<div class="beers form col-md-10 columns content">
    <br>
    <h1 class="page-title"><?= $beer->beer_name ?></h1>
    <?php
        $optImg=[
        'link'=>'Usar um link externo',
        'upload'=>'Fazer upload da imagem'
        ];

        if($beer->average_rating != null){
            $beer->average_rating = number_format($beer->average_rating, 2, '.', '');
        }

    ?>
    <?php if($beer->image != '' && $beer->image_src_type == 'upload'):?>
        <div class="img-container">
          <?= $this->Html->image('uploads/beer_images/'.$beer->image, ["class"=>"beer-image"]); ?>
        </div>
    <?php elseif($beer->image_link != '' && $beer->image_src_type == 'link'):?>
        <div class="img-container">
          <?= $this->Html->image($beer->image_link, ["class"=>"beer-image"]); ?>
        </div>
    <?php endif;?>

    <br>
    <p class="red">Os campos com * são obrigatórios</p>
    <?= $this->Form->create($beer,['type' => 'file']) ?>
    <fieldset>
        <div class="row">
            <div class="col-md-5">
                <?= $this->Form->input('beer_name',['required'=>'required','label'=>['text'=>'Nome da Cerveja:','class'=>'mandatory']]); ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 inline-flex-add">

                    <?= $this->Form->input('barcodes.0.barcode',['label'=>['text'=>'Código de Barras:']]); ?>

                    <?= $this->Form->input('barcodes.0.id',['type'=>'hidden']); ?>

                    <input type="hidden" value='0' class="barcode_position_number">

                    <i onclick="addField()" class="glyphicon glyphicon-plus icon-add-remove-fields" aria-hidden="true"></i>

            </div>


            <div class="col-md-6">
                <?= $this->Form->input('manufacturer_name',['label'=>['text'=>'Nome do fabricante:']]); ?>
            </div>

        </div>


        <div class="row barcode-fields">

            <?php

                $quantidade_de_codigos = count($beer['barcodes']);

                if($quantidade_de_codigos > 1){
                    $labelNumber = 2;
                    foreach ($beer['barcodes'] as $key => $barcode) {
                        // echo $key;
                        if($key != 0){ // evitando repetir o campo no indice 0
                            // incremento a key pra ser diferente do primeiro campo padrão 0

                           echo '<div class="fieldset-'.$barcode['id'].' col-md-6 inline-flex-add ">';

                           echo '<div class="form-group text"><label for="barcode">Código de Barras '.$labelNumber.':</label>';

                           echo '<input type="text" value="'.$barcode['barcode'].'" name="barcodes['.$key.'][barcode]" id="barcode-'.$key.'" class="form-control">';

                           echo '<input type="hidden" value="'.$barcode['id'].'" name="barcodes['.$key.'][id]" id="barcode-'.$key.'" class="form-control">';

                           echo '<input type="hidden" value="'.$labelNumber.'" class="barcode_position_number">';

                           echo '</div>';

                           echo '<i class="glyphicon glyphicon-minus icon-add-remove-fields" onclick="removeDB('.$barcode['id'].')"  aria-hidden="true"></i>';

                           echo   '</div>';

                           $labelNumber++;
                        }


                    }
                }
            ?>



        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('average_rating',['required'=>'required','class'=>'decimal mini-field','type'=>'text','label'=>['class'=>'mandatory','text'=>'Nota de avaliação:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('evaluations_total',['class'=>'mini-field','type'=>'text','label'=>['text'=>'Número de avaliadores:']]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('alcohol_content',['class'=>'decimal3 mini-field','label'=>['text'=>'Àlcool (%ABU):']]); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('bitterness',['required'=>'required','class'=>'mini-field','label'=>['class'=>'mandatory','text'=>'Amargor (IBU):']]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('average_price',['class'=>'decimal3 mini-field','type'=>'text','label'=>['text'=>'Preço médio:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('packing',['class'=>'medium-field','label'=>['text'=>'Embalagem(ml):']]);?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                 <?= $this->Form->input('country',['class'=>'medium-field','label'=>['text'=>'País:']]); ?>
            </div>
            <div class="col-md-6">
                 <?= $this->Form->input('state',['class'=>'medium-field','label'=>['text'=>'Estado:']]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('city',['class'=>'medium-field','label'=>['text'=>'Cidade:']]); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('colour',['class'=>'medium-field','label'=>['text'=>'Cor (visual):']]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('ideal_temperature',['class'=>' graus mini-field','label'=>['text'=>'Temperatura (C°)']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('calories',['class'=>'decimal3 mini-field','type'=>'text','label'=>['text'=>'Calorias (cal):']]); ?>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('glass_type',['class'=>'medium-field','label'=>['text'=>'Copo ideal']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('seasonality',['class'=>'medium-field','label'=>['text'=>'Sazonalidade:']]); ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->input('harmonization',['type'=>'textarea','label'=>['text'=>'Harmonização sugerida:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('comercial_presentation',['label'=>['text'=>'Descrição comercial:']]); ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('family',['class'=>'medium-field','label'=>['text'=>'Família:']]);?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('style',['class'=>'mini-field','label'=>['text'=>'Estilo:']]); ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('brewery_partner_group',['class'=>'medium-field','label'=>['text'=>'Parceria:']]); ?>
            </div>

            <div class="col-md-6">
                <?= $this->Form->input('importer',['class'=>'medium-field','label'=>['text'=>'Importador/Distribuidor']]); ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <?= $this->Form->input('history',['class'=>'medium-field','label'=>['text'=>'Histórico']]); ?>
            </div>

            <div class="col-md-6">

                 <?= $this->Form->input('image',['class'=>'imageUpload','type'=>'file','label'=>['class'=>'imageUpload','text'=>'Imagem:']]); ?>

                <!-- <?= $this->Form->input('image_src_type',['class'=>'medium-field','options'=>$optImg,'type'=>'select','label'=>['text'=>'Tipo de imagem:']]); ?> -->

            </div>

        </div>
        <div class="row">

            <div class="col-md-6">

                <!-- <?= $this->Form->input('image_link',['class'=>'imageLink hidden-field','type'=>'text','label'=>['class'=>'imageLink hidden-field','text'=>'Link da Imagem:']]); ?> -->

                <?= $this->Form->input('lastImage',['type'=>'hidden']); ?>
            </div>
        </div>
        <div style="display:none;" class="deletionDiv">

        </div>


    </fieldset>
    <button type="button" class="btn btn-default red-buttons" onclick="goBack()">Voltar</button>
    <button type="submit" class="btn btn-default yellow-buttons">Salvar</button>
    <?= $this->Form->end() ?>
    <br><br>
</354erc354erc354erc354ercdiv>

<script>

    <?php if($beer->image != ''):?>
        var imageName = "<?=$beer->image?>";
        $("#lastimage").attr('value',imageName);
    <?php endif;?>


    // $("#image-src-type").change(function(){

    //     console.log($(this).val());
    //     if($(this).val() == 'link'){
    //         $('.imageLink').removeClass("hidden-field");
    //         $('.imageLink').attr('disabled',false);
    //         $('.imageUpload').addClass("hidden-field");
    //         $('.imageUpload').attr('disabled',true);
    //     }else{
    //         $('.imageUpload').removeClass("hidden-field");
    //         $('.imageUpload').attr('disabled',false);
    //         $('.imageLink').addClass("hidden-field");
    //         $('.imageLink').attr('disabled',true);
    //     }

    // });

    // if($("#image-src-type").val() == 'link'){
    //     $('.imageLink').removeClass("hidden-field");
    //     $('.imageLink').attr('disabled',false);
    //     $('.imageUpload').addClass("hidden-field");
    //     $('.imageUpload').attr('disabled',true);
    // }else{
    //     $('.imageUpload').removeClass("hidden-field");
    //     $('.imageUpload').attr('disabled',false);
    //     $('.imageLink').addClass("hidden-field");
    //     $('.imageLink').attr('disabled',true);
    // }




    jQuery(function($){
       $(".decimal").mask("9.99", {reverse: true});
       $(".decimal3").maskMoney({thousands:'', decimal:'.', allowZero:true});


       $("#image").change(function(){
            $("#lastimage").val($(this).val());
       });

    });

    var barcode_position_number = 0;

    $('.barcode_position_number').each(function(){
        var position_number = parseInt($(this).val());
        console.log('posição: '+position_number);
        barcode_position_number = (position_number > barcode_position_number) ? barcode_position_number = position_number : barcode_position_number = barcode_position_number;
    });

    console.log('last: '+barcode_position_number);

    var cont = (barcode_position_number == 0) ? 1 : barcode_position_number;
    var fieldsetNumber = cont+1;
    var html = '';

    function addField(){
      html =
            '<div class="fieldset-'+fieldsetNumber+' col-md-6 inline-flex-add ">'+
            '<div class="form-group text"><label for="barcode">Código de Barras '+fieldsetNumber+':</label>'+
            '<input type="text" name="barcodes['+cont+'][barcode]" id="barcode-'+cont+'" class="form-control"></div>'+
            '<i class="glyphicon glyphicon-minus icon-add-remove-fields" onclick="removeElem('+fieldsetNumber+')"  aria-hidden="true"></i>'+
            '</div>';

      $('.barcode-fields').append(html);
      $('.barcode-fields').slideDown();
      cont++;
      fieldsetNumber++;

    }

    function removeDB(number){
        if(number){
            cont--;
            fieldsetNumber--;
            $(".fieldset-"+number).remove();
            $(".deletionDiv").append('<input type="hidden" name="deleteArray[]" value="'+number+'">');
        }
    }

    function removeElem(number){
        if(number){
            cont--;
            fieldsetNumber--;
            $(".fieldset-"+number).remove();
        }
    }


</script>
