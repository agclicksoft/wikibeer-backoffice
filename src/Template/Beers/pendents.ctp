
<div class="row">

<div class="beers index col-md-12 columns content">

    <div class="buttons">
        <?= $this->Html->link(__('Cadastrar'), ['action' => 'add'], ['class'=>'btn yellow-buttons new-beer btn-default']) ?>
        <button class="btn yellow-buttons btn-default import-button" data-toggle="modal" data-target="#myModal">Importar</button>

        <?= $this->Html->link(__('Para aprovar'), ['action' => 'pendents'], ['class'=>'btn yellow-buttons new-beer btn-default', 'style' => 'margin-left: 4%']) ?>
    </div>

    <?= $this->Form->create('index', ['type'=>'get','class'=>'search-form']) ?>
  
      <div class="form-group">
       
       <input type="text" id="campo-busca" class="form-control inline-field small-field" placeholder="Digite aqui sua busca" name="busca">

       <select type="text" name="tipo_busca" placeholder="status" id="field-search" class="form-control aluno-search inline-field mini-field">
           <option value="">Selecione o tipo de busca</option>

           <option value="nome">Nome</option>

           <option value="barcode">Código de barras</option>
       </select>
       
       <?= $this->Form->button(__('Filtrar',true))?>
      
      </div>

    <?= $this->Form->end() ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <!-- <th><?= $this->Paginator->sort('id') ?></th> -->
                <th class="left-align"><?= $this->Paginator->sort('beer_name','Cervejas',['direction' => 'desc']) ?></th>
                <th class="actions right-align"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($beers as $beer): 

                $edit_img   =  $this->Html->image('edit-icon.gif', ['class' => 'action-icons']); 
                $delete_img =  $this->Html->image('delete-icon.png', ['class' => 'action-icons']);
            ?>
            <tr>
                <td class="left-align"><?= h($beer->beer_name) ?></td>
                <td class="actions right-align" style="white-space:nowrap">
                    <?= $this->Html->link($edit_img, ['action' => 'edit', $beer->id], ['escape'=>false]) ?>

                    <?= $this->Form->postLink($delete_img, ['action' => 'deactivate', $beer->id],['escape'=>false]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <center>
        <div class="paginator">

                <ul class="pagination">
                    <?= $this->Paginator->prev('&laquo; ' . __('anterior'), ['escape'=>false]) ?>
                    <?= $this->Paginator->numbers(['escape'=>false]) ?>
                    <?= $this->Paginator->next(__('próximo') . ' &raquo;', ['escape'=>false]) ?>
                </ul>
                <!-- <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
             {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p> -->
        </div>
    </center>
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Importar Cervejas</h4>
      </div>
      
      <div style="padding: 40px;">
          <?= $this->Form->create('index', ['id'=>'import-form','type'=>'file','url'=>'/beers/import']) ?>
            <?= $this->Form->input('csv',['required'=>'required','type'=>'file','label'=>['text'=>'Selecione o arquivo para importação:','id'=>'csv-input']]); ?>
            <button id="subButton" type="submit" onclick="this.form.submit()"  class="btn btn-default yellow-buttons">Iniciar importação</button>
            <br>
            <div class="hide-loader" style="display: none">
                 <div class="loader hide-loader" ></div><span>Importando, por favor aguarde...</span>
            </div>
           
          <?= $this->Form->end() ?>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn close-modal btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    $('document').ready(function(){
        
        $('.close-modal').click(function(){
            $('#import-form')[0].reset();
            $('.hide-loader').hide();
            $('#subButton').show();
            $('#subButton').attr('disabled',false);
        });

        $('#subButton').click(function(){
            $(this).attr('disabled',true);
            $(this).hide();
            $('.hide-loader').show();
        });
        
    });
    
    

</script>