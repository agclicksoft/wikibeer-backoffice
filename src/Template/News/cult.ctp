<?php
$edit_img = $this->Html->image('edit-icon.gif', ['class' => 'action-icons']);
$delete_img = $this->Html->image('delete-icon.png', ['class' => 'action-icons']);
?>
<div class="row">
    <div class="col-md-12">
        <div class="buttons">
            <?=$this->Html->link(__('Novo Cult'), ['action' => 'cultAdd'], ['class' => 'btn yellow-buttons new-beer btn-default'])?>
            <?=$this->Html->link(__('Categorias'), ['controller' => 'news-categories', 'action' => 'index'], ['class' => 'btn yellow-buttons new-beer btn-default'])?>
        </div>
    </div>
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="left-align">Título</th>
                    <th class="right-align">Última modificação</th>
                    <th class="right-align">Publicada</th>
                    <th class="actions right-align"><?=__('Ações')?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($news as $n): ?>
                    <tr>
                        <td class="left-align"><?=$n->title?></td>
                        <td class="right-align"><?=$n->modified->format('d/m/Y H:i:s')?></td>
                        <td class="right-align"><?=$n->visible ? 'Sim' : 'Não'?></td>
                        <td class="actions right-align" style="white-space:nowrap">
                            <?=$this->Html->link($edit_img, ['action' => 'edit', $n->id], ['escape' => false])?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <center>
            <div class="paginator">
                <ul class="pagination">
                    <?=$this->Paginator->prev('&laquo; ' . __('anterior'), ['escape' => false])?>
                    <?=$this->Paginator->numbers(['escape' => false])?>
                    <?=$this->Paginator->next(__('próximo') . ' &raquo;', ['escape' => false])?>
                </ul>
            </div>
        </center>
    </div>
</div>