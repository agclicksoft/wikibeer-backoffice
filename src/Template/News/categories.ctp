<?php
$edit_img   =  $this->Html->image('edit-icon.gif', ['class' => 'action-icons']); 
$delete_img =  $this->Html->image('delete-icon.png', ['class' => 'action-icons']);
?>
<div class="row">
    <div class="col-md-12">
        <div class="buttons">
            <?= $this->Html->link(__('Nova notícia'), ['action' => 'add'], ['class'=>'btn yellow-buttons new-beer btn-default']) ?>
            <?= $this->Html->link(__('Categorias'), ['action' => 'categories'], ['class'=>'btn yellow-buttons new-beer btn-default']) ?>
        </div>
    </div>
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="left-align">Nome</th>
                    <th class="right-align">Subcategorias</th>
                    <th class="actions right-align"><?= __('Ações') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($categories as $category): ?>
                    <tr>
                        <td class="left-align"><?=$category->name?></td>
                        <td class="right-align"><?=count($category->news_subcategories)?></td>
                        <td class="actions right-align" style="white-space:nowrap">
                            <?=$this->Html->link($edit_img, ['action' => 'categoryEdit', $category->id], ['escape'=>false]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <center>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('&laquo; ' . __('anterior'), ['escape'=>false]) ?>
                    <?= $this->Paginator->numbers(['escape'=>false]) ?>
                    <?= $this->Paginator->next(__('próximo') . ' &raquo;', ['escape'=>false]) ?>
                </ul>
            </div>
        </center>
    </div>
</div>