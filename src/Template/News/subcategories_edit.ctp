<div class="row">
    <div class="col-md-12">
        <br>
        <h1 class="page-title"><?= 'Editar Categoria' ?></h1>
    </div>
    <div class="col-md-12">
        <?= $this->Form->create($subcategory,['type' => 'file']) ?>
        <div class="row">
            <div class="col-md-6">
                <?=$this->Form->input('name', ['label'=>['text' => 'Nome:', 'class' => 'mandatory'], 'required' => 'required'])?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="mandatory" for="category-id">Categoria:</label>
                    <?=$this->Form->select('category_id',  $categories, ['id' => 'category-id', 'required' => 'required', 'empty' => 'Selecione'])?>
                </div>
            </div>
        </div>

        <button type="button" class="btn btn-default red-buttons" onclick="goBack()">Voltar</button>
        <?=$this->Form->button('Salvar', ['type' => 'submit', 'class' => 'btn btn-default yellow-buttons']) ?>
        
    </div>
    <?= $this->Form->end() ?>
</div>