<div class="row">
    <div class="col-md-12">
        <br>
        <h1 class="page-title"><?= 'Nova categoria' ?></h1>
        <br>
        <p class="red">Os campos com * são obrigatórios</p>
        <br>
        <?= $this->Form->create($subcategory) ?>
    </div>
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-4">
          <?=$this->Form->input('name', ['label'=>['text' => 'Nome:', 'class' => 'mandatory'], 'required' => 'required'])?>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label class="mandatory" for="subcategory-id">Categoria:</label>
            <?=$this->Form->select('category_id', $categories, ['id' => 'category-id', 'required' => 'required'])?>
          </div>
        </div>
      </div>
      <button type="button" class="btn btn-default red-buttons" onclick="goBack()">Voltar</button>
      <?=$this->Form->button('Salvar', ['type' => 'submit', 'class' => 'btn btn-default yellow-buttons']) ?>
      </div>
      <?= $this->Form->end() ?>
</div>