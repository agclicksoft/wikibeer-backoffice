<div class="row">
    <div class="col-md-12">
        <br>
        <h1 class="page-title"><?='Novo Cult'?></h1>
    </div>
    <div class="col-md-12">
        <?=$this->Form->create($new, ['type' => 'file'])?>
        <div class="row">
            <div class="col-md-5">
                <?=$this->Form->input('title', ['label' => ['text' => 'Título:', 'class' => 'mandatory'], 'required' => 'required'])?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="mandatory" for="category-id">Categoria:</label>
                    <?=$this->Form->select('category_id', $categories, ['id' => 'category-id', 'required' => 'required', 'empty' => 'Selecione'])?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="visible">Publicado:</label>
                    <?=$this->Form->select('visible', ['Não', 'Sim'], ['id' => 'visible', 'required' => 'required'])?>
                </div>
            </div>
            <div class="col-md-3">
              <?=$this->Form->input('image', ['class' => 'imageUpload', 'type' => 'file', 'label' => ['class' => 'imageUpload', 'text' => 'Imagem:']]);?>
            </div>
            <div class="col-md-12" id="content-field" style="display: none;">
                <?=$this->Form->textarea('content', ['id' => 'content'])?>
            </div>
        </div>

        <button type="button" class="btn btn-default red-buttons" onclick="goBack()">Voltar</button>
        <?=$this->Form->button('Salvar', ['type' => 'submit', 'class' => 'btn btn-default yellow-buttons'])?>

        <?=$this->Form->end()?>
    </div>
</div>
<?=$this->Html->script('tinymce/tinymce.min');?>
<style>
.tox-statusbar__branding {
    /* Hide TiniMCE Brand*/
    display: none;
}
</style>
<script>
  $(document).ready(function () {

    $('#category-id').change(function () {
        let categoryId = $('#category-id').val();
        let contentField = $('#content-field');
        if (categoryId.length > 0) {
            contentField.show();
        } else {
            contentField.hide();
        }
    });

  });

tinymce.init({
    language: 'pt_BR',
    //plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
    plugins: 'searchreplace autolink directionality visualblocks visualchars fullscreen image link media template table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
    toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',
    selector: '#content'
  });
</script>