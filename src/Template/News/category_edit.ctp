<div class="row">
    <div class="col-md-12">
        <br>
        <h1 class="page-title"><?= 'Editar Categoria' ?></h1>
    </div>
    <div class="col-md-12">
        <?= $this->Form->create($news,['type' => 'file']) ?>
        <div class="row">
            <div class="col-md-6">
              
                <?=$this->Form->input('name', ['label'=>['text' => 'Categoria:', 'class' => 'mandatory'], 'required' => 'required'])?>
            </div>
          
            <div class="col-md-2">
                <div class="form-group">
                    <label class="mandatory" for="subcategory-id">Subcategoria:</label>
                    <?=$this->Form->select('subcategory_id', ['' => 'Selecione'] + $subcategories, ['id' => 'subcategory-id', 'required' => 'required'])?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="visible">Publicado:</label>
                    <?=$this->Form->select('visible', ['Não', 'Sim'], ['id' => 'visible', 'required' => 'required'])?>
                </div>
            </div>
            <div class="col-md-12" id="content-field">
                <?= $this->Form->textarea('content', ['id' => 'content']) ?>
            </div>
        </div>

        <button type="button" class="btn btn-default red-buttons" onclick="goBack()">Voltar</button>
        <?=$this->Form->button('Salvar', ['type' => 'submit', 'class' => 'btn btn-default yellow-buttons']) ?>

        <?= $this->Form->end() ?>
    </div>
</div>
<?=$this->Html->script('tinymce/tinymce.min');?>
<style>
.tox-statusbar__branding {
    /* Hide TiniMCE Brand*/
    display: none;
}
</style>
<script>
$(document).ready(function () {
    $('#category-id').change(function () {
        $('#content-field').hide();
        let categoryId = $(this).val();
        $('#subcategory-id').empty();
        $('#subcategory-id').append('<option value="">Carregando...</option>');
        $.get(BASEURL + 'news/listSubcategories/' + categoryId, function (data) {
            let subcategories = '<option value="">Selecione</option>';
            if (data.subcategories) {
                for (let key in data.subcategories) {
                    subcategories += '<option value="' + key + '">' + data.subcategories[key] + '</option>';
                }
            }
            $('#subcategory-id').empty();
            $('#subcategory-id').append(subcategories);
        });
    });
    $('#subcategory-id').change(function () {
        let categoryId = $('#category-id').val();
        let subcategoryId = $(this).val();

        let contentField = $('#content-field');
        
        if (categoryId.length > 0 && subcategoryId.length > 0) {
            contentField.show();
        } else {
            contentField.hide();
        }
    });
});

tinymce.init({
    language: 'pt_BR',
    //plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
    plugins: 'searchreplace autolink directionality visualblocks visualchars fullscreen image link media template table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
    toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',
    selector: '#content'
  });
</script>