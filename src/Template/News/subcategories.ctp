<?php
    $edit_img   =  $this->Html->image('edit-icon.gif', ['class' => 'action-icons']); 
    $delete_img =  $this->Html->image('delete-icon.png', ['class' => 'action-icons']);
?>
<div class="row">
    <div class="col-md-12">
        <div class="buttons">
            <?= $this->Html->link(__('Nova Categoria'), ['action' => 'subcategoriesAdd'], ['class'=>'btn yellow-buttons new-beer btn-default']) ?>
        </div>
    </div>
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="left-align">Nome</th>
                    <th class="right-align">Categoria</th>
                    <th class="right-align">Ações</th>
                    <th class="actions right-align">
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($subcategories as $subcategory): ?>
                    <tr>
                        <td class="left-align"><?=$subcategory->name?></td>
                        <td class="right-align"><?=$subcategory->news_category->name?></td>
                        <td class="exibitions right-align">
                                
                        </td>
                        <td class="actions right-align" style="white-space:nowrap">
                          <?=$this->Html->link($edit_img, ['action' => 'subcategoriesEdit', $subcategory->id], ['escape'=>false]) ?>
                          <?= $this->Form->postLink($delete_img, ['action' => 'subcategoriesDelete', $subcategory->id],['escape'=>false]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>

                 


</div>
        </table>
        <center>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('&laquo; ' . __('anterior'), ['escape'=>false]) ?>
                    <?= $this->Paginator->numbers(['escape'=>false]) ?>
                    <?= $this->Paginator->next(__('próximo') . ' &raquo;', ['escape'=>false]) ?>
                </ul>
            </div>
        </center>
    </div>
</div>
<?= $this->Form->end() ?>
<script>
    $(document).ready( function() {
      $('#select').change( function() {
          location.href = $(this).val();
      });
  });
</script>
