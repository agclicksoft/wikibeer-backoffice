<?php
$edit_img = $this->Html->image('edit-icon.gif', ['class' => 'action-icons']);
$delete_img = $this->Html->image('delete-icon.png', ['class' => 'action-icons']);
?>
<div class="row">
    <div class="col-md-12">
        <div class="buttons">
            <?=$this->Html->link(__('Nova Categoria'), ['action' => 'add'], ['class' => 'btn yellow-buttons new-beer btn-default'])?>
        </div>
    </div>
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th class="left-align">Categoria</th>
                    <th class="left-align">Modo de exibição</th>
                    <th class="actions right-align">
                    </th>
                    <th class="right-align">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($newsCategories as $category): ?>
                    <tr>
                        <td class="left-align"><?=$category->title?></td>
                        <td class="left-align"><?=$category->category_type == 'cult' ? 'Cult' : 'News'?></td>
                        <td class="exibitions right-align">
                        </td>
                        <td class="actions right-align" style="white-space:nowrap">
                          <?=$this->Html->link($edit_img, ['action' => 'edit', $category->id], ['escape' => false])?>
                          <?=$this->Form->postLink($delete_img, ['action' => 'delete', $category->id], ['escape' => false])?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </div>
        </table>
        <center>
            <div class="paginator">
                <ul class="pagination">
                    <?=$this->Paginator->prev('&laquo; ' . __('anterior'), ['escape' => false])?>
                    <?=$this->Paginator->numbers(['escape' => false])?>
                    <?=$this->Paginator->next(__('próximo') . ' &raquo;', ['escape' => false])?>
                </ul>
            </div>
        </center>
    </div>
</div>
<?=$this->Form->end()?>
<script>
    $(document).ready( function() {
      $('#select').change( function() {
          location.href = $(this).val();
      });
  });
</script>
