<div class="row">
    <div class="col-md-12">
        <br>
        <h1 class="page-title"><?='Nova categoria'?></h1>
        <br>
        <p class="red">Os campos com * são obrigatórios</p>
        <br>
        <?=$this->Form->create($newsCategory, ['type' => 'file'])?>
    </div>
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-4">
          <?=$this->Form->input('title', ['label' => ['text' => 'Nome:', 'class' => 'mandatory'], 'required' => 'required'])?>
        </div>
        <div class="col-md-4">
          <?=$this->Form->input('subtitle', ['label' => ['text' => 'Subtitulo:', 'class' => 'mandatory'], 'required' => 'required'])?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
        <label class="mandatory" for="category-id">Modo de exibição:</label>
        <?=$this->Form->select('category_type', ['cult' => 'Cult', 'news' => 'News'], ['id' => 'category-type', 'required' => 'required', 'empty' => 'Selecione'])?>
        </div>
        <div class="col-md-4">
        <?=$this->Form->input('image', ['class' => 'imageUpload', 'type' => 'file', 'label' => ['class' => 'imageUpload', 'text' => 'Imagem:']]);?>
        </div>
      </div>
      <button type="button" class="btn btn-default red-buttons" onclick="goBack()">Voltar</button>
      <?=$this->Form->button('Salvar', ['type' => 'submit', 'class' => 'btn btn-default yellow-buttons'])?>
      </div>
      <?=$this->Form->end()?>
</div>