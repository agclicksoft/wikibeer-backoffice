<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * Upload component
 */

class UploadComponent extends Component
{

    public function send($data, $entity)
    {
        $beers_table = TableRegistry::get('Beers');

        if (!empty($data['image']['name'])) {

            $file = $data['image'];

            $filename = $file['name'];
            $file_tmp_name = $file['tmp_name'];
            $dir = WWW_ROOT . 'img' . DS . 'uploads/beer_images';
            $allowed = array('png', 'jpg', 'jpeg');

            if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {

                // throw new InternalErrorException("Error Processing Request", 1);

                return $beers_table->save($entity);

            } elseif (is_uploaded_file($file_tmp_name)) {

                $filename = Text::uuid() . '-' . $filename;

                move_uploaded_file($file_tmp_name, $dir . DS . $filename);

                $entity->image = $filename;

                return $beers_table->save($entity);
            }

        }

        return $beers_table->save($entity);
    }

    public function saveCategorieWithImage($data, $entity)
    {
        $newCategoriesTable = TableRegistry::get('NewsCategories');

        if (!empty($data['image']['name'])) {

            $file = $data['image'];

            $filename = $file['name'];
            $file_tmp_name = $file['tmp_name'];
            $dir = WWW_ROOT . 'img' . DS . 'uploads/categories_images';
            $allowed = array('png', 'jpg', 'jpeg');

            if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {

                // throw new InternalErrorException("Error Processing Request", 1);

                return $newCategoriesTable->save($entity);

            } elseif (is_uploaded_file($file_tmp_name)) {

                $filename = Text::uuid() . '-' . $filename;

                move_uploaded_file($file_tmp_name, $dir . DS . $filename);

                $entity->image = $filename;

                return $newCategoriesTable->save($entity);
            }

        }

        return $newCategoriesTable->save($entity);
    }

    public function saveNewWithImage($data, $entity)
    {
        $newsTable = TableRegistry::get('News');

        if (!empty($data['image']['name'])) {

            $file = $data['image'];

            $filename = $file['name'];
            $file_tmp_name = $file['tmp_name'];
            $dir = WWW_ROOT . 'img' . DS . 'uploads/news_images';
            $allowed = array('png', 'jpg', 'jpeg');

            if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {

                // throw new InternalErrorException("Error Processing Request", 1);

                return $newsTable->save($entity);

            } elseif (is_uploaded_file($file_tmp_name)) {

                $filename = Text::uuid() . '-' . $filename;

                move_uploaded_file($file_tmp_name, $dir . DS . $filename);

                $entity->image = $filename;

                return $newsTable->save($entity);
            }

        }

        return $newsTable->save($entity);
    }

}
