<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Breweries Controller
 *
 * @property \App\Model\Table\BreweriesTable $Breweries */
class BreweriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->set('title', 'BeerSpy');

        $query = $this->request->getQueryParams();

        $conditions = [
            'Breweries.status' => 1,
        ];

        if (!empty($query['type']) && $query['type'] == 'bars') {
            $conditions['Breweries.type'] = 1;
            $type = 'bars';
        } else {
            $conditions['Breweries.type'] = 0;
            $type = 'breweries';
        }

        if (isset($this->request->query['busca'])) {
            $conditions['Breweries.name LIKE'] = '%' . $this->request->query['busca'] . '%';
        }

        $breweries = $this->Breweries->find('all', [
            'conditions' => $conditions,
        ]);

        $breweries = $this->paginate($breweries);

        $this->set(compact('breweries', 'type'));
        $this->set('_serialize', ['breweries']);
    }

    /**
     * View method
     *
     * @param string|null $id Brewery id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $brewery = $this->Breweries->get($id, [
            'contain' => [],
        ]);

        $this->set('brewery', $brewery);
        $this->set('_serialize', ['brewery']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $query = $this->request->getQueryParams();
        if (!empty($query['type']) && $query['type'] == 'bars') {
            $title = 'Cadastrar Bar/Pub';
            $name = 'O Bar/Pub';
            $type = 1;
        } else {
            $title = 'Cadastrar Cervejaria';
            $name = 'A Cervejaria/Microcervejaria';
            $type = 0;
        }

        $this->set('title', $title);
        $brewery = $this->Breweries->newEntity();
        if ($this->request->is('post')) {
            $brewery = $this->Breweries->patchEntity($brewery, $this->request->getData());
            $brewery->status = 1;

            if (empty($brewery->email)) {
                $brewery->email = null;
            }

            if ($this->Breweries->save($brewery)) {
                $this->Flash->success(__($name . ' foi salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__($name . ' não pode ser salva, por favor tente novamente.'));
        }
        $this->set(compact('brewery', 'title', 'type'));
        $this->set('_serialize', ['brewery']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Brewery id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Editar Cervejaria');
        $brewery = $this->Breweries->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $requestData = $this->request->getData();
            $brewery = $this->Breweries->patchEntity($brewery, $requestData);

            if (isset($requestData['image_changed']) && $requestData['image_changed'] == '1') {
                if (isset($requestData['new_image_link']) && strlen($requestData['new_image_link']) > 0) {
                    if (strpos($requestData['new_image_link'], 'data:') !== false) {
                        $b64image = substr($requestData['new_image_link'], strpos($requestData['new_image_link'], ',') + 1);
                        $images_src = 'img/uploads/breweries_images/' . uniqid();
                        if (strpos($requestData['new_image_link'], 'image/jpeg') !== false) {
                            $images_src .= '.jpg';
                        } elseif (strpos($requestData['new_image_link'], 'image/png') !== false) {
                            $images_src .= '.png';
                        } elseif (strpos($requestData['new_image_link'], 'image/gif') !== false) {
                            $images_src .= '.gif';
                        }

                        if (file_put_contents(WWW_ROOT . $images_src, base64_decode($b64image)) !== false) {
                            $brewery->image_link = "http://login.wikibeer.com.br/{$images_src}";
                        }
                    }
                }
            }

            if ($this->Breweries->save($brewery)) {
                $this->Flash->success(__('A cervejaria foi editada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A cervejaria não pode ser editada, por favor tente novamente.'));
        }
        $this->set(compact('brewery'));
        $this->set('_serialize', ['brewery']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Brewery id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $brewery = $this->Breweries->get($id);
        if ($this->Breweries->delete($brewery)) {
            $this->Flash->success(__('The brewery has been deleted.'));
        } else {
            $this->Flash->error(__('The brewery could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deactivate($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $brewery = $this->Breweries->get($id);
        $brewery->status = 0;
        if ($this->Breweries->save($brewery)) {
            $this->Flash->success(__('A cervejaria foi removida da listagem com sucesso.'));
        } else {
            $this->Flash->error(__('A cervejaria não pôde ser removida da listagem, por favor tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function searchBreweries($type = null)
    {
        $this->render(false);      

        $query = $this->request->getQueryParams();

        $filters = ['Breweries.status' => 1];
        if ($type !== null) {
            $filters['Breweries.type ='] = $type;
        }
        if (isset($query['name'])) {
            $filters['Breweries.name LIKE'] = "%{$query['name']}%";
        }
        if (isset($query['city'])) {
            $filters['Breweries.city LIKE'] = "%{$query['city']}%";
        }
        if (isset($query['state'])) {
            $filters['Breweries.state LIKE'] = "%{$query['state']}%";
        }

        $breweries = $this->Breweries->find('all', [
            'conditions' => $filters,
        ]);

        $this->paginate = [
            'limit' => 20
        ];
        $breweries = $this->paginate($breweries);
        //$breweries = $breweries->limit(60);

        foreach ($breweries as $brewery) {
            $brewery->typeName = $brewery->getTypeName();
        }

        $this->response->body(json_encode($breweries));
        $this->response->type('application/json');
        return $this->response;
    }

    public function findBreweryById($id = null)
    {
        $this->render(false);
        $brewery = [];
        if ($id !== null) {
            $brewery = $this->Breweries->get($id);
            $brewery->typeName = $brewery->getTypeName();
        }

        $this->response->body(json_encode($brewery));
        $this->response->type('application/json');
        return $this->response;
    }

    public function findBreweryByType($typeId = null)
    {
        $this->render(false);
        $breweries = $this->Breweries->find()
            ->where(['Breweries.type' => $typeId]);

        $this->paginate = [
            'limit' => 20
        ];
        $breweries = $this->paginate($breweries);

        foreach ($breweries as $brewery) {
            $brewery->typeName = $brewery->getTypeName();
        }

        $this->response->body(json_encode($breweries));
        $this->response->type('application/json');
        return $this->response;
    }

    public function submitBrewery()
    {
        $this->render(false);

        $response = [
            'status' => 'failed',
            'message' => 'error'
        ];

        if ($this->request->is('post')) {
            $requestData = $this->request->getData();
            $brewery = $this->Breweries->newEntity();
            $brewery = $this->Breweries->patchEntity($brewery, $requestData);
            $brewery->status = 0;
            $brewery->approved = 0; //0 = Pendente | 1=Aprovado | 2=Reprovado


            if ($this->Breweries->save($brewery)) {
                $response['status'] = 'success';
                $response['message'] = 'Salvo com sucesso.';
            }
        }

        if ($response['status'] == 'failed') {
            $this->response->withStatus(400);
        }

        $this->response->body(json_encode($response));
        $this->response->type('application/json');
        return $this->response;
    }

    public function saveRate()
    {
        $this->render(false);
        $results['success'] = false;

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (!empty($data['breweryId']) && !empty($data['rating_score'])) {
                $rating = $this->Breweries->BreweryRatings->newEntity();
                $rating->brewery_id = $data['breweryId'];
                $rating->rating_score = $data['rating_score'];

                if ($this->Breweries->BreweryRatings->save($rating)) {
                    $brewery = $this->Breweries->get($rating->brewery_id, ['contain' => ['BreweryRatings']]);
                    $brewery['evaluations_total'] = $brewery['evaluations_total'] + 1;

                    $totalOfRates = 0;
                    $numberOfRates = count($brewery['brewery_ratings']);
                    if ($numberOfRates > 0) {
                        foreach ($brewery["brewery_ratings"] as $key => $rating) {
                            $totalOfRates += $rating['rating_score'];
                        }

                        $average_rating = $totalOfRates / $numberOfRates;
                        $brewery['average_rating'] = number_format($average_rating, 2, '.', '');

                        if ($this->Breweries->save($brewery)) {
                            $results['success'] = true;
                            $results['brewery'] = $brewery;
                        }
                    }
                }
            }
        }

        $this->response->body(json_encode($results));
        $this->response->type('application/json');
        return $this->response;
    }
}
