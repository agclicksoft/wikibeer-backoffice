<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * Beers Controller
 *
 * @property \App\Model\Table\BeersTable $Beers
 */
class BeersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Upload');
        $this->set('title', 'Cervejas');
    }

    public function numberOfRatings($array = null)
    {

        $ratings_table = TableRegistry::get('Ratings');

        // contar o número de avaliações para cada cerveja
        foreach ($array as $key => $beer) {

            $query = $ratings_table->find('all', [
                'conditions' => ['Ratings.beer_id LIKE' => $beer->id],
            ]);

            $evaluations_total = $query->count();

            $beer->evaluations_total += $evaluations_total;

        }

        return $array;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $this->set('title', 'BeerSpy');

        $param = $this->request;
        $busca = null;
        $tipo_busca = null;
        $authorizations = null;

        if (isset($param->query['tipo_busca']) && isset($param->query['busca'])) {

            $busca = $param->query['busca'];
            $tipo_busca = $param->query['tipo_busca'];

            switch ($tipo_busca) {

                case 'nome':

                    $query = $this->Beers->find('all', [
                        'contain' => ['Barcodes', 'Ratings'],
                        'conditions' => [
                            'Beers.beer_name LIKE' => '%' . $busca . '%',
                            'Beers.status' => 1,
                        ],

                    ]);

                    break;

                case 'barcode':

                    $barcodeObject = $this->Beers->Barcodes->find('all', [
                        'conditions' => [
                            'Barcodes.barcode' => $busca,
                        ],
                    ])->first();

                    if (!empty($barcodeObject)) {
                        $query = $this->Beers->find('all', [
                            'conditions' => [
                                'Beers.id' => $barcodeObject->beer_id,
                            ],
                            'contain' => ['Ratings', 'Barcodes'],
                        ]);
                    } else {
                        $query = $this->Beers->find('all', [
                            'conditions' => [
                                'Beers.id' => '',
                            ],
                            'contain' => ['Ratings', 'Barcodes'],
                        ]);
                    }

                    // echo'<pre>';
                    // print_r($query);
                    // echo'</pre>';

                    break;

                default:

                    $query = $this->Beers->find('all')
                        ->where(['Beers.status' => 1])
                        ->contain(['Barcodes', 'Ratings']);

                    break;
            }

        } else {

            $query = $this->Beers->find('all')
                ->where(['Beers.status' => 1])
                ->contain(['Barcodes', 'Ratings']);
        }

        $beers = $this->paginate($query);

        $beers = $this->numberOfRatings($beers);

        $this->set(compact('beers'));
        $this->set('_serialize', ['beers']);
    }

    public function searchBeers($busca = null)
    {
        //caminho para teste no ar
        //$serverImgFolder = "http://login.beerspy.com.br/img/uploads/beer_images/";
        $serverImgFolder = Router::url('/', true) . 'img/uploads/beer_images/';

        // caminho para teste local
        // $serverImgFolder = "http://localhost:8765/img/uploads/beer_images/";

        $this->render(false);
        $filters = [];

        if ($busca != null) {
            $filters['Beers.beer_name LIKE'] = "%$busca%";
        }

        $beers = $this->Beers->find('all', [
            'conditions' => $filters,
            'contain' => ['Ratings', 'Barcodes'],
        ]);

        $this->paginate = [
            'limit' => 20
        ];
        $beers = $this->paginate($beers)->toArray();
            // ->limit(60)
            // ->toArray();

        $found = count($beers);

        if ($found == 0) {
            $beers = [];
            $barcodeObject = $this->Beers->Barcodes->find('all', [
                'conditions' => [
                    'Barcodes.barcode' => $busca,
                ],
            ])->first();

            if (!empty($barcodeObject)) {
                $beer = $this->Beers->get($barcodeObject->beer_id, ['contain' => ['Ratings', 'Barcodes']]);
                $beers[0] = $beer;
            }
        }

        foreach ($beers as $key => $beer) {

            // obtendo a media das avaliações da cerveja
            $totalOfRates = 0;
            $numberOfRates = count($beer["ratings"]);

            if ($numberOfRates > 0) {

                foreach ($beer["ratings"] as $key => $rating) {
                    $totalOfRates += $rating['rating_score'];
                }

                $average_rating = $totalOfRates / $numberOfRates;
                $beer->average_rating = number_format($average_rating, 2, '.', '');

            }
            // obtendo a media das avaliações

            if ($beer['image'] != '' && $beer['image'] != null) {
                $beer['image'] = $serverImgFolder . $beer['image'];
            }

        }

        $this->response->body(json_encode($beers));
        $this->response->type('application/json');
        return $this->response;
    }

    public function findByBarcode($barcode = null)
    {

        $beer = null;
        $dataArray = null;
        $imgName = '';
        // caminho para teste no ar
        //$serverImgFolder = "http://login.beerspy.com.br/img/uploads/beer_images/";
        $serverImgFolder = Router::url('/', true) . 'img/uploads/beer_images/';

        // caminho para teste local
        // $serverImgFolder = "http://localhost:8765/img/uploads/beer_images/";

        $this->render(false);

        $barcodeObject = $this->Beers->Barcodes->find()
            ->select(['Barcodes.beer_id'])
            ->where(['Barcodes.barcode' => $barcode])
            ->first();

        if (!empty($barcodeObject)) {

            $beer = $this->Beers->get($barcodeObject->beer_id, ['contain' => ['Ratings', 'Barcodes']]);

            // getting the evaluations average
            $totalOfRates = 0;
            $numberOfRates = count($beer->ratings);

            if ($numberOfRates > 0) {

                foreach ($beer["ratings"] as $key => $rating) {
                    $totalOfRates += $rating['rating_score'];
                }

                $average_rating = $totalOfRates / $numberOfRates;
                $beer->average_rating = number_format($average_rating, 2, '.', '');

            }

            // checking if we got an image
            if ($beer['image'] != '' && $beer['image'] != null) {
                $imgName = $beer['image'];
            }

            //increment beer visits
            $beer['visits'] = $beer['visits'] + 1;
            $dataArray = [];

            if ($this->Beers->save($beer)) {
                // concatenating the image path to render in app
                if ($imgName != '') {
                    $beer['image'] = $serverImgFolder . $imgName;
                }

                array_push($dataArray, $beer);
            }

        }

        $this->response->body(json_encode($dataArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function findById($id = null)
    {

        $this->render(false);
        $beer = null;
        $dataArray = null;
        $imgName = '';

        // path to test on air
        //$serverImgFolder = "http://login.beerspy.com.br/img/uploads/beer_images/";
        $serverImgFolder = Router::url('/', true) . 'img/uploads/beer_images/';

        // path to test local
        // $serverImgFolder = "http://localhost:8765/img/uploads/beer_images/";

        $beer = $this->Beers->get($id, ['contain' => ['Ratings', 'Barcodes']]);

        // getting the evaluations average
        $totalOfRates = 0;
        $numberOfRates = count($beer->ratings);

        if ($numberOfRates > 0) {

            foreach ($beer["ratings"] as $key => $rating) {
                $totalOfRates += $rating['rating_score'];
            }

            $average_rating = $totalOfRates / $numberOfRates;
            $beer->average_rating = number_format($average_rating, 2, '.', '');

        }

        if ($beer['image'] != '' && $beer['image'] != null) {
            $imgName = $beer['image'];
        }

        //increment beer visits
        $beer['visits'] = $beer['visits'] + 1;
        $dataArray = [];

        if ($this->Beers->save($beer)) {
            // concatenating the image path to render in app
            if ($imgName != '') {
                $beer['image'] = $serverImgFolder . $imgName;
            }
            array_push($dataArray, $beer);
        }

        $this->response->body(json_encode($dataArray));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * View method
     *
     * @param string|null $id Beer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $beer = $this->Beers->get($id, [
            'contain' => ['Barcodes', 'Ratings'],
        ]);

        $this->set('beer', $beer);
        $this->set('_serialize', ['beer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Cadastrar Cerveja');
        $beer = $this->Beers->newEntity();

        if ($this->request->is('post')) {

            $beer = $this->Beers->patchEntity($beer, $this->request->getData());

            if ($this->Upload->send($this->request->data, $beer)) {
                $this->Flash->success(__('A cerveja foi salva com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('A cerveja não pode ser salva, por favor tente novamente.'));
        }
        $this->set(compact('beer'));
        $this->set('_serialize', ['beer']);
    }

    public function saveRate()
    {
        $this->render(false);
        $results['success'] = false;

        // path to test on air
        //$serverImgFolder = "http://login.beerspy.com.br/img/uploads/beer_images/";
        $serverImgFolder = Router::url('/', true) . 'img/uploads/beer_images/';

        // path to test local
        // $serverImgFolder = "http://localhost:8765/img/uploads/beer_images/";

        if ($this->request->is('post')) {

            $data = $this->request->getData();

            if ($data['beerId'] != '' || $data['beerId'] != null) {

                $rating = $this->Beers->Ratings->newEntity();
                $rating->beer_id = $data['beerId'];
                $rating->rating_score = $data['rating_score'];

                if ($this->Beers->Ratings->save($rating)) {

                    $beer = $this->Beers->get($rating->beer_id, ['contain' => ['Ratings']]);

                    // incrementando o numero de avaliadores
                    $beer['evaluations_total'] = $beer['evaluations_total'] + 1;

                    // obtendo a media das avaliações inicio
                    $totalOfRates = 0;
                    $numberOfRates = count($beer->ratings);

                    if ($numberOfRates > 0) {

                        foreach ($beer["ratings"] as $key => $rating) {
                            $totalOfRates += $rating['rating_score'];
                        }

                        $average_rating = $totalOfRates / $numberOfRates;
                        $beer->average_rating = number_format($average_rating, 2, '.', '');

                        if ($this->Beers->save($beer)) {

                            $results['success'] = true;

                            if ($beer['image'] != '' && $beer['image'] != null) {
                                $beer['image'] = $serverImgFolder . $beer['image'];
                            }

                            $results['beer'] = $beer;
                        }
                    }

                }
            }

        }

        $this->response->body(json_encode($results));
        $this->response->type('application/json');
        return $this->response;
    }

    public function saveSuggestion()
    {
        $suggestions_table = TableRegistry::get('Suggestions');
        $this->render(false);
        $results['success'] = false;
        $suggestion = null;

        if ($this->request->is('post')) {

            $data = $this->request->getData();

            if ($data['suggestion'] != '' || $data['suggestion'] != null) {

                $exists = $suggestions_table->exists(['beer_name' => $data['suggestion']]);

                if ($exists) {

                    $existingSuggestion = $suggestions_table->find('all', [
                        'conditions' => [
                            'Suggestions.beer_name' => $data['suggestion'],
                        ],
                    ])->first();

                    $existingSuggestion->suggestions_total += 1;

                    if ($suggestions_table->save($existingSuggestion)) {
                        $results['success'] = true;
                    }

                } else {

                    $suggestion = $suggestions_table->newEntity();
                    $suggestion->beer_name = $data['suggestion'];
                    $suggestion->suggestions_total = 1;

                    if ($suggestions_table->save($suggestion)) {
                        $results['success'] = true;
                    }
                }

            }

        }

        $this->response->body(json_encode($results));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Edit method
     *
     * @param string|null $id Beer id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Editar Cerveja');
        $barcodeIds = [];
        $barcodes_table = TableRegistry::get('Barcodes');

        $beer = $this->Beers->get($id, [
            'contain' => ['Barcodes', 'Ratings'],
        ]);

        $lastImage = $beer->image;
        //dump($beer);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $beer = $this->Beers->patchEntity($beer, $this->request->getData());
            // echo "<pre>";
            // print_r($beer);
            // echo "</pre>";
            // exit;
            //delete barcodes
            if (!empty($beer['deleteArray'])) {
                $barcodeIds = $beer['deleteArray'];
                $barcodes_table->deleteAll(['Barcodes.id IN' => $barcodeIds]);
            }
            
            $hasImage = false;
            if (!empty($this->request->getData()['image'])) {
                $img = $this->request->getData()['image'];
                if (is_array($img) && count($img) > 0 && $img['size'] > 0) {
                    $hasImage = true;
                }
            }
            
            if (/*$lastImage != $beer->$lastImage || */$hasImage) {
                if ($this->Upload->send($this->request->data, $beer)) {
                    $this->Flash->success(__('A cerveja foi editada com sucesso1.'));

                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $beer->image = $lastImage;
                if ($this->Beers->save($beer)) {
                    $this->Flash->success(__('A cerveja foi editada com sucesso2.'));

                    return $this->redirect(['action' => 'index']);
                }
            }

            $this->Flash->error(__('A cerveja não pode ser editada, por favor tente novamente.'));
        }

        $this->set(compact('beer'));
        $this->set('_serialize', ['beer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Beer id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $beer = $this->Beers->get($id);
        if ($this->Beers->delete($beer)) {
            $this->Flash->success(__('The beer has been deleted.'));
        } else {
            $this->Flash->error(__('The beer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deactivate($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $beer = $this->Beers->get($id);
        $beer->status = 0;
        if ($this->Beers->save($beer)) {
            $this->Flash->success(__('A cerveja foi removida da listagem com sucesso.'));
        } else {
            $this->Flash->error(__('A cerveja não pode ser removida da listagem, por favor tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function treatBeerErrors($array = null)
    {
        $error_string = '';

        foreach ($array['beer_names'] as $key => $value) {
            $error_string .= $value . ',';
        }

        $error_string = trim($error_string, ",");
        return $error_string;

    }

    public function getImage($url)
    {

        $status = false;
        $image_name = '';
        $ext = '.png';

        $serverImgFolder = WWW_ROOT . "/img/uploads/beer_images/";

        // if is valid image
        if (@getimagesize($url)) {

            //getting image name
            $image_name = pathinfo($url)['filename'] . $ext;

            // path and name concatenation
            $copyPath = $serverImgFolder . $image_name;

            //getting image resource and converting to png
            $status = imagepng(imagecreatefromstring(file_get_contents($url)), $copyPath, 1);

        }

        $result['image_name'] = $image_name;
        $result['status'] = $status;

        return $result;

    }

    public function import()
    {

        define("LNBR", PHP_EOL);

        $this->render(false);

        // creating an array with the possible model associations
        $associated = [
            'Barcodes',
            'Ratings',
        ];

        if ($this->request->is('post')) {

            //getting the file from post
            $data = $this->request->data['csv'];
            $file = $data['tmp_name'];
            $filename = $data['name'];
            $allowed = array('csv');
            $errosOnSave = null;

            // verifying file extension
            if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {

                $this->Flash->error(__('O tipo de arquivo importado não é compatível, por favor utilize a extensão ".csv".'));
                return $this->redirect($this->referer());

            }

            $handle = fopen($file, "r");

            //initializing my beers array and my iterator
            $beers = [];
            $i = 0;

            // cycle through the rows in the csv file
            while (($row = fgetcsv($handle, 3000, ","))) {

                $mediaInicial = 0;

                // checking if row is empty and the number of columns is correct
                if ($row == '' || empty($row) || $row == null || count($row) != 25) {
                    // echo count($row);
                    break;
                }

                // checking if name is empty
                if ($row[0] == '' || empty($row[0])) {
                    break;
                }

                // creating a ratings array to association
                $ratings = null;
                if (!empty($row[1])) {
                    $row[1] = str_replace(",", ".", $row[1]);
                    $mediaInicial = (is_numeric($row[1])) ? $row[1] : 0;
                    $ratings[0] = [
                        'rating_score' => (is_numeric($row[1])) ? $row[1] : 0,
                    ];
                }

                // creating a barcodes array to association, in case it is a string with more than one barcode we transform it into an array
                $barcodes = null;
                if (!empty($row[3])) {
                    $barcodeString = trim($row[3], ",");
                    $barcodesArray = explode(',', $barcodeString);
                    foreach ($barcodesArray as $key => $value) {
                        $value = trim($value);
                        $barcodes[$key] = [
                            'barcode' => $value,
                        ];
                    }
                }

                $beers[$i] = [
                    'beer_name' => $row[0],
                    'ratings' => $ratings,
                    'average_rating' => $mediaInicial,
                    'evaluations_total' => (is_numeric($row[2])) ? $row[2] : 0,
                    'barcodes' => $barcodes,
                    'family' => $row[4],
                    'style' => $row[5],
                    'country' => $row[6],
                    'city' => $row[7],
                    'state' => $row[8],
                    'manufacturer_name' => $row[9],
                    'brewery_partner_group' => $row[10],
                    'importer' => $row[11],
                    'alcohol_content' => $row[12],
                    'bitterness' => $row[13],
                    'colour' => $row[14],
                    'glass_type' => $row[15],
                    'ideal_temperature' => $row[16],
                    'calories' => (is_numeric($row[17])) ? $row[17] : 0,
                    'image_link' => $row[18],
                    'image_src_type' => 'upload',
                    'average_price' => $row[19],
                    'harmonization' => $row[20],
                    'history' => $row[21],
                    'seasonality' => $row[22],
                    'packing' => $row[23],
                    'comercial_presentation' => $row[24],

                ];

                $i++;

            }

            // echo "<pre>";
            // print_r($beers[0]);
            // echo "<pre>";
            // exit;

            // checking my beers array and starting to save each entity
            if (!empty($beers)) {

                unset($beers[0]); // elimininado o header de titulos do csv

                $beersToSave = count($beers);
                $success = 0;
                $errosOnSave['beer_names'] = [];
                $errosOnSave['modelErrors'] = [];

                foreach ($beers as $key => $beer) {

                    $exists = $this->Beers->exists(['beer_name' => $beer['beer_name']]);

                    if (!$exists) {
                        $beerObject = $this->Beers->newEntity();
                        $beerObject = $this->Beers->patchEntity($beerObject, $beer, ['associated' => $associated]);

                        // get image
                        $img_url = $beerObject->image_link;

                        $imageResult = $this->getImage($img_url);

                        if ($imageResult['status']) {
                            $beerObject->image = $imageResult['image_name'];
                            $beerObject->image_src_type = 'upload';
                        } else {
                            $beerObject->image_link = '';
                            $beerObject->image = '';
                        }
                        // get image

                        if ($this->Beers->save($beerObject)) {
                            $success++;
                        } else {
                            array_push($errosOnSave['modelErrors'], $beerObject->errors());
                            Log::write('debug', $beerObject->beer_name);
                            Log::write('debug', $beerObject->errors());
                        }
                    } else {
                        array_push($errosOnSave['beer_names'], $beer['beer_name']);
                    }

                }

                // erros

                if (!empty($errosOnSave['beer_names'])) {
                    //$beerNamesString = $this->treatBeerErrors($errosOnSave);
                    // $beerNamesString stores the name of beers not imported by repetition
                    $this->Flash->error(__('Alguns registros possuiam nomes que já existiam e não foram importados.'));
                }

                if (!empty($errosOnSave['modelErrors'])) {
                    $this->Flash->error(__('Alguns registros não foram importados. Verifique o preenchimento dos campos e tente novamente.'));
                }

                // sucesso
                if ($success == $beersToSave) {
                    $this->Flash->success(__('A importação foi feita com sucesso.'));
                }

            } else {
                $this->Flash->error(__('O arquivo de importação enviado estava vazio ou não foi preenchido corretamente.'));
            }

            //closing the csv file handler and redirecting back to my last url
            fclose($handle);
            return $this->redirect(['action' => 'index']);
        }
    }

    public function submitBeer()
    {
        $this->render(false);

        $response = [
          'status' => 'failed',
          'message' => 'error'
        ];

        if ($this->request->is('post')) {
            $requestData = $this->request->getData();
            $brewery = $this->Beers->newEntity();
            $brewery = $this->Beers->patchEntity($brewery, $requestData);
            $brewery->status = 0;
            $brewery->approved = 0; //0 = Pendente | 1=Aprovado | 2=Reprovado

            if ($this->Beers->save($brewery)) {
                $response['status'] = 'success';
                $response['message'] = 'Salvo com sucesso.';
            }
        }

        if ($response['status'] == 'failed') {
            $this->response->withStatus(400);
        }

        $this->response->body(json_encode($response));
        $this->response->type('application/json');
        return $this->response;
    }

    public function pendents()
    {
        $this->set('title', 'BeerSpy');

        $beers = $this->Beers->find()->where(['approved' => 0]);

        $this->set(compact('beers'));
        $this->set('_serialize', ['beers']);
    }
}
