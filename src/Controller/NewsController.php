<?php
namespace App\Controller;
use Cake\Routing\Router;
use App\Controller\AppController;

/**
 * News Controller
 *
 * @property \App\Model\Table\NewsTable $News */
class NewsController extends AppController
{

    protected $associated = [
        'NewsCategories',
        'NewsSubcategories',
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Upload');
        $this->set('title', 'Notícias');
        $this->Auth->allow([
            'listByCategory',
            'listByCategoryType',
            'getNewById',
        ]);
    }

    public function getNewById($id = null)
    {
        $this->autoRender = false;
        $new = $this->News->get($id, [
            'contain' => [],
        ]);

        $this->response->body(json_encode($new));
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * list method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    public function listByCategory($categoryId = null)
    {

        $this->autoRender = false;
        $serverImgFolder = Router::url('/', true) . 'img/uploads/news_images/';

        $news = $this->News->find()->where(['News.visible' => true])->order(['News.created' => 'DESC']);

        $news->matching('NewsCategories', function ($q) use ($categoryId) {
            return $q->where(['NewsCategories.id' => $categoryId]);
        });

        $newsArray = $news->toArray();
        foreach ($newsArray as $key => $new) {
            if ($new['image']) {
                $new['image'] = $serverImgFolder . $new['image'];
            }
        }

        $this->response->body(json_encode($newsArray));
        $this->response->type('application/json');
        return $this->response;
    }

    public function listByCategoryType($categoryType = 'news')
    {

        $this->autoRender = false;
        $serverImgFolder = Router::url('/', true) . 'img/uploads/news_images/';
        $news = $this->News->find()->where(['News.visible' => true])->order(['News.created' => 'DESC']);

        $news->matching('NewsCategories', function ($q) use ($categoryType) {
            return $q->where(['NewsCategories.category_type' => $categoryType]);
        });

        $newsArray = $news->toArray();
        foreach ($newsArray as $key => $new) {
            if ($new['image']) {
                $new['image'] = $serverImgFolder . $new['image'];
            }
        }

        $this->response->body(json_encode($newsArray));
        $this->response->type('application/json');
        return $this->response;
    }

    private function getNewsCategoriesIds($type = 'news')
    {
      $newsIds = $this->News->NewsCategories->find('list')->where(['category_type' => $type])->toArray();
      return $newsIds = array_keys($newsIds);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $newsIds = $this->getNewsCategoriesIds();
        
        $news = $this->News->find()->where(['category_id IN' => $newsIds])->order(['News.created' => 'DESC']);
        $news = $this->paginate($news);

        $this->set(compact('news'));
        $this->set('_serialize', ['news']);
    }

    public function cult()
    {
        $cultIds = $this->getNewsCategoriesIds('cult');

        $news = $this->News->find()->where(['category_id IN' => $cultIds])->order(['News.created' => 'DESC']);
        
        $news = $this->paginate($news);

        $this->set(compact('news'));
        $this->set('_serialize', ['news']);
    }

    /**
     * View method
     *
     * @param string|null $id News id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $news = $this->News->get($id, [
            'contain' => [''],
        ]);

        $this->set('news', $news);
        $this->set('_serialize', ['news']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $new = $this->News->newEntity();

        $categories = $this->News->NewsCategories->find('list')
        ->where(['category_type' => 'news'])->toArray();

        if ($this->request->is('post')) {
            $new = $this->News->patchEntity($new, $this->request->getData());
            if ($this->Upload->saveNewWithImage($this->request->data, $new)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The news could not be saved. Please, try again.'));
        }
        $this->set(compact('new', 'categories'));
        $this->set('_serialize', ['new']);
    }

    public function cultAdd()
    {
        $new = $this->News->newEntity();

        $categories = $this->News->NewsCategories->find('list')
        ->where(['category_type' => 'cult'])->toArray();

        if ($this->request->is('post')) {
            $new = $this->News->patchEntity($new, $this->request->getData());
            if ($this->Upload->saveNewWithImage($this->request->data, $new)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The news could not be saved. Please, try again.'));
        }
        $this->set(compact('new', 'categories'));
        $this->set('_serialize', ['new']);
    }

    /**
     * Edit method
     *
     * @param string|null $id News id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $new = $this->News->get($id, [
            'contain' => ['NewsCategories'],
        ]);
        $newsCategory = $new->news_category;

        $lastImage = $new->image;
        $type = $new->news_category->category_type;

        $categories = $this->News->NewsCategories->find('list')->where(['category_type' => $type])->toArray();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $new = $this->News->patchEntity($new, $this->request->getData());
            $hasImage = !empty($this->request->getData()['image']);
            if ($lastImage != $new->lastImage || $hasImage) {
                if ($this->Upload->saveNewWithImage($this->request->data, $new)) {
                    $this->Flash->success(__('A notícia foi editada com sucesso.'));
                } else {
                    $this->Flash->error(__('A notícia não pode ser editada. Por favor tente novamente.'));
                }
            } else {
                $new->image = $lastImage;
                if ($this->News->save($new)) {
                    $this->Flash->success(__('A notícia foi editada com sucesso.'));
                } else {
                    $this->Flash->error(__('A notícia não pode ser editada. Por favor tente novamente.'));
                }
            }

            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('new', 'categories', 'type', 'newsCategory'));
        $this->set('_serialize', ['new']);
    }

    /**
     * Delete method
     *
     * @param string|null $id News id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        /*

        $this->request->allowMethod(['post', 'delete']);
        $beer = $this->Beers->get($id);
        $beer->status = 0;
        if ($this->Beers->save($beer)) {
        $this->Flash->success(__('A cerveja foi removida da listagem com sucesso.'));
        } else {
        $this->Flash->error(__('A cerveja não pode ser removida da listagem, por favor tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);

         */
        $this->request->allowMethod(['post', 'delete']);
        $news = $this->News->get($id);
        if ($this->News->delete($news)) {
            $this->Flash->success(__('The news has been deleted.'));
        } else {
            $this->Flash->error(__('The news could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function subcategories()
    {
        $subcategories = $this->News->NewsCategories->NewsSubcategories->find()
            ->contain(['NewsCategories']);
        $subcategories = $this->paginate($subcategories);

        $this->set(compact('subcategories'));
        $this->set('_serialize', ['subcategories']);
    }

    public function subcategoriesAdd()
    {
        $subcategory = $this->News->NewsCategories->NewsSubcategories->newEntity(null, ['contain' => ['NewsCategories']]);

        $categories = $this->News->NewsCategories->find('list', [
            'fields' => ['NewsCategories.id', 'NewsCategories.title'],
        ]);
        $this->set(compact('news', 'categories', 'subcategory'));
        $this->set('_serialize', ['news']);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $requestData = $this->request->getData();

            $subcategory = $this->News->NewsCategories->NewsSubcategories->patchEntity($subcategory, $requestData, ['associated' => ['NewsCategories']]);

            if ($this->News->NewsCategories->NewsSubcategories->save($subcategory, ['associated' => ['NewsCategories']])) {
                $this->Flash->success(__('Subcategoria salva com sucesso.'));
                return $this->redirect(['action' => 'subcategories']);
            }
            $this->Flash->error(__('Não foi possivel salvar a subcategoria, tente novamente.'));
        }
    }

    public function subcategoriesEdit($id = null)
    {
        $subcategory = $this->News->NewsCategories->NewsSubcategories->get($id, [
            'contain' => ['NewsCategories'],
        ]);
        $categories = $this->News->NewsCategories->find('list', [
            'fields' => ['NewsCategories.id', 'NewsCategories.title'],
        ]);
        $this->set(compact('news', 'categories', 'subcategory'));
        $this->set('_serialize', ['news']);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $requestData = $this->request->getData();

            $subcategory = $this->News->NewsCategories->NewsSubcategories->patchEntity($subcategory, $requestData);

            if ($this->News->NewsCategories->NewsSubcategories->save($subcategory)) {
                $this->Flash->success(__('Subcategoria salva com sucesso.'));
                return $this->redirect(['action' => 'subcategories']);
            }
            $this->Flash->error(__('Não foi possivel salvar a subcategoria, tente novamente.'));
        }
    }

    public function subcategoriesDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subcategory = $this->News->NewsSubcategories->get($id);
        if ($this->News->NewsSubcategories->delete($subcategory)) {
            $this->Flash->success(__('Categoria deletada com sucesso.'));
        } else {
            $this->Flash->error(__('Categoria não foi deletada, tente novamente.'));
        }

        return $this->redirect(['action' => 'subcategories']);
    }

    public function listSubcategories($categoryId = null)
    {
        $this->render(false);
        $subcategories = $this->News->NewsSubcategories->find('list')->where(['category_id' => $categoryId]);
        $subcategories = ['subcategories' => $subcategories];
        $this->response->body(json_encode($subcategories));
        $this->response->type('application/json');
        return $this->response;
    }

    public function listNews()
    {
        $this->render(false);
        $news_ = $this->News->find()->where(['visible' => 1])->order(['News.created' => 'DESC']);

        $news = [];
        foreach ($news_ as $n) {
            $news[] = [
                'id' => $n->id,
                'title' => $n->title,
                'created' => $n->created,
                'modified' => $n->modified,
                'content' => $n->content,
            ];
        }

        $news = ['news' => $news];
        $this->response->body(json_encode($news));
        $this->response->type('application/json');
        return $this->response;
    }

}
