<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Suggestions Controller
 *
 * @property \App\Model\Table\SuggestionsTable $Suggestions
 */
class SuggestionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
         $this->set('title','Sugestões');  
        $suggestions = $this->paginate($this->Suggestions,[
            'order' => [
            'Suggestions.suggestions_total' => 'desc'
        ]]);

        $this->set(compact('suggestions'));
        $this->set('_serialize', ['suggestions']);
    }

    /**
     * View method
     *
     * @param string|null $id Suggestion id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $suggestion = $this->Suggestions->get($id, [
            'contain' => []
        ]);

        $this->set('suggestion', $suggestion);
        $this->set('_serialize', ['suggestion']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $suggestion = $this->Suggestions->newEntity();
        
        if ($this->request->is('post')) {
            
            $suggestion = $this->Suggestions->patchEntity($suggestion, $this->request->getData());
            
            $query = $this->Suggestions->find('all',[
                'conditions'=>['beer_name'=>$suggestion->beer_name]
            ]);

            if($query->count() > 0){
                
                $suggestion = $query->first();
                
                $suggestion->suggestions_total += 1;

                $this->Suggestions->save($suggestion);

                $this->Flash->success(__('Total de sugestões incrementada.'));

                return $this->redirect(['action' => 'index']);

            }else{

                $suggestion->suggestions_total += 1;

                if ($this->Suggestions->save($suggestion)) {
                    $this->Flash->success(__('Sugestão salva com sucesso'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('A sugestão não pode ser salva.'));
            }
        
        }
        $this->set(compact('suggestion'));
        $this->set('_serialize', ['suggestion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Suggestion id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $suggestion = $this->Suggestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $suggestion = $this->Suggestions->patchEntity($suggestion, $this->request->getData());
            if ($this->Suggestions->save($suggestion)) {
                $this->Flash->success(__('The suggestion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suggestion could not be saved. Please, try again.'));
        }
        $this->set(compact('suggestion'));
        $this->set('_serialize', ['suggestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Suggestion id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $suggestion = $this->Suggestions->get($id);
        if ($this->Suggestions->delete($suggestion)) {
            $this->Flash->success(__('A sugestão foi deletada com sucesso.'));
        } else {
            $this->Flash->error(__('A sugestão não foi deletada. Por favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
