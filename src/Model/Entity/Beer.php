<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Beer Entity
 *
 * @property int $id
 * @property string $beer_name
 * @property string $manufacturer_name
 * @property float $alcohol_content
 * @property string $bitterness
 * @property string $harmonization
 * @property string $style
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $colour
 * @property string $glass_type
 * @property float $calories
 * @property float $ideal_temperature
 * @property string $image
 * @property float $average_price
 * @property string $comercial_presentation
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $status
 *
 * @property \App\Model\Entity\Barcode[] $barcodes
 * @property \App\Model\Entity\Rating[] $ratings
 */
class Beer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
