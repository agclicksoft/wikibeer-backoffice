<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Brewery Entity
 *
 * @property int $id
 * @property int $type
 * @property string $name
 * @property string $zipcode
 * @property string $street
 * @property string $complement
 * @property string $neighborhood
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $telephone
 * @property string $email
 * @property string $website
 * @property string $facebook
 * @property string $instagram
 * @property string $image_link
 */class Brewery extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function getTypeName()
    {
        $names = [
            'Cervejaria e Microcervejaria',
            'Bar e Pubs',
        ];

        if (array_key_exists($this->type, $names)) {
            return $names[$this->type];
        }
        return '';
    }

    public function getImageUrl()
    {
        if (empty($this->image_link) || $this->image_link == null) {
            return "http://login.wikibeer.com.br/img/placeholder.png";
        }
        return $this->image_link;
    }
}
