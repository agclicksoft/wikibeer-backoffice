<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Breweries Model
 *
 * @method \App\Model\Entity\Brewery get($primaryKey, $options = [])
 * @method \App\Model\Entity\Brewery newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Brewery[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Brewery|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Brewery patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Brewery[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Brewery findOrCreate($search, callable $callback = null, $options = [])
 */ class BreweriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('breweries');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('BreweryRatings', [
            'foreignKey' => 'brewery_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')->allowEmpty('id', 'create');
        $validator
            ->integer('type')->requirePresence('type', 'create')->notEmpty('type');
        $validator
            ->requirePresence('name', 'create')->notEmpty('name');
        $validator
            ->allowEmpty('zipcode');
        $validator
            ->allowEmpty('street');
        $validator
            ->allowEmpty('complement');
        $validator
            ->allowEmpty('neighborhood');
        $validator
            ->allowEmpty('city');
        $validator
            ->allowEmpty('state');
        $validator
            ->allowEmpty('country');
        $validator
            ->allowEmpty('telephone');
        $validator
            ->email('email')->allowEmpty('email');
        $validator
            ->allowEmpty('website');
        $validator
            ->allowEmpty('facebook');
        $validator
            ->allowEmpty('instagram');
        $validator
            ->allowEmpty('image_link');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
