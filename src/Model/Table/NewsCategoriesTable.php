<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewsCategories Model
 *
 * @method \App\Model\Entity\NewsCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewsCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewsCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewsCategory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewsCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewsCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewsCategory findOrCreate($search, callable $callback = null, $options = [])
 */class NewsCategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('news_categories');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->hasMany('News', [
          'foreignKey' => 'category_id',
        ]);

        $this->hasMany('NewsSubcategories', [
            'foreignKey' => 'category_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')->allowEmpty('id', 'create');
        $validator
            ->requirePresence('title', 'create')->notEmpty('title');
        return $validator;
    }
}
