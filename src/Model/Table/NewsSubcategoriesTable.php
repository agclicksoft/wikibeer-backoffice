<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewsSubcategories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $NewsCategories
 *
 * @method \App\Model\Entity\NewsSubcategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewsSubcategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewsSubcategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewsSubcategory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewsSubcategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewsSubcategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewsSubcategory findOrCreate($search, callable $callback = null, $options = [])
 */class NewsSubcategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('news_subcategories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        /*$this->hasOne('NewsCategories', [
            'foreignKey' => false,
            'conditions' => 'NewsCategories.id = NewsSubcategories.category_id',
        ]);*/
        $this->belongsTo('NewsCategories', [
          'foreignKey' => 'category_id'
      ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        $validator
            ->requirePresence('name', 'create')            ->notEmpty('name');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['category_id'], 'NewsCategories'));

        return $rules;
    }
}
