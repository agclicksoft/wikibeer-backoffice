<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BreweryRatings Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Breweries
 *
 * @method \App\Model\Entity\BreweryRating get($primaryKey, $options = [])
 * @method \App\Model\Entity\BreweryRating newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BreweryRating[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BreweryRating|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BreweryRating patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BreweryRating[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BreweryRating findOrCreate($search, callable $callback = null, $options = [])
 */
class BreweryRatingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('brewery_ratings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Breweries', [
            'foreignKey' => 'brewery_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('rating_score')
            ->requirePresence('rating_score', 'create')
            ->notEmpty('rating_score');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['brewery_id'], 'Breweries'));

        return $rules;
    }
}
