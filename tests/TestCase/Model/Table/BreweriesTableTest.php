<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BreweriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BreweriesTable Test Case
 */
class BreweriesTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\BreweriesTable     */
    public $Breweries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.breweries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Breweries') ? [] : ['className' => 'App\Model\Table\BreweriesTable'];        $this->Breweries = TableRegistry::get('Breweries', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Breweries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
