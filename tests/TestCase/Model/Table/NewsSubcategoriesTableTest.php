<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewsSubcategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewsSubcategoriesTable Test Case
 */
class NewsSubcategoriesTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\NewsSubcategoriesTable     */
    public $NewsSubcategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.news_subcategories',
        'app.news_categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewsSubcategories') ? [] : ['className' => 'App\Model\Table\NewsSubcategoriesTable'];        $this->NewsSubcategories = TableRegistry::get('NewsSubcategories', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewsSubcategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
