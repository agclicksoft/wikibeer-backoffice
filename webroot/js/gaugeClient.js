﻿// data which need to be fetched

// var need_obj = {
//   value: 0,
//   gaugeMaxValue: 100,
//   percentValue: 0
// }

var name = "azerty";

var value = 0;


var gaugeMaxValue = 100; 

// données à calculer 
var percentValue = value / gaugeMaxValue; 

////////////////////////

var needleClient;




(function(){

var barWidth, chart, chartInset, degToRad, repaintGauge,
    height, margin, numSections, padRad, percToDeg, percToRad, 
    percent, radius, sectionIndx, svg, totalPercent, width,
    valueText, formatValue, k;

  percent = percentValue;

  numSections = 1;
  sectionPerc = 1 / numSections / 2;
  padRad = 0.025;
  chartInset = 10;
    
  // Orientation of gauge:
  totalPercent = .75;

  el = d3.select('.chart-gauge');

  margin = {
    top: 30,
    right: 30,
    bottom: 30,
    left: 30
  };

  width = el[0][0].offsetWidth - margin.left - margin.right;
  height = width;
  radius = Math.min(width, height) / 2;
  barWidth = 40 * width / 300;



  reajustNeedle = function(val) {
  	value = val;
	percentValue = value / gaugeMaxValue; 
	percent = percentValue;
    needle.moveTo(percent);
  };
  
  //Utility methods 
  
  percToDeg = function(perc) {
    return perc * 360;
  };

  percToRad = function(perc) {
    return degToRad(percToDeg(perc));
  };

  degToRad = function(deg) {
    return deg * Math.PI / 180;
  };

  //var span = '<span>OPAaofk</span>';
  var span = '<span>OPAaofk';
  // Create SVG element
  svg = el.append('svg').attr('width', width + margin.left + margin.right).attr('height', (height/2) + margin.top + margin.bottom);


  // Add layer for the panel
  chart = svg.append('g').attr('transform', "translate(" + ((width) / 2 + margin.left) + ", " + ((height + margin.top) / 2) + ")");


  chart.append('path').attr('class', "arc chart-first");
  chart.append('path').attr('class', "arc chart-second");
  chart.append('path').attr('class', "arc chart-third");
  chart.append('path').attr('class', "arc chart-fourth");
    
  valueText = chart.append("text")
                    .attr('id', "Value")
                    .attr("font-size",16)
                    .attr("text-anchor","middle")
                    .attr("dy",".5em")
                    .style("fill", '#000000');
  formatValue = d3.format('1%');

  arc4 = d3.svg.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth)
  arc3 = d3.svg.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth)
  arc2 = d3.svg.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth)
  arc1 = d3.svg.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth)

  repaintGauge = function () 
  {
    perc = 0.5;
    var next_start = totalPercent;
    arcStartRad = percToRad(next_start);
    arcEndRad = arcStartRad + percToRad(perc / 5);
    next_start += perc / 5;

    //arc1
    arc1.startAngle(arcStartRad).endAngle(arcEndRad);

    //arc2
    arcStartRad = percToRad(next_start);
    arcEndRad = arcStartRad + percToRad(perc / 2.5);
    next_start += perc / 2.5;
    arc2.startAngle(arcStartRad + padRad).endAngle(arcEndRad);

    

    //arc3
    arcStartRad = percToRad(next_start);
    arcEndRad = arcStartRad + percToRad(perc / 5);
    next_start += perc / 3.5;
    arc3.startAngle(arcStartRad + padRad).endAngle(arcEndRad);


    //arc4
    arcStartRad = percToRad(next_start / 8);
    arcEndRad = arcStartRad + percToRad(perc / 5);
    arc4.startAngle(arcStartRad + padRad).endAngle(arcEndRad);
    
    chart.select(".chart-first").attr('d', arc1);
    chart.select(".chart-second").attr('d', arc2);
    chart.select(".chart-third").attr('d', arc3);
    chart.select(".chart-fourth").attr('d', arc4);

  }
/////////

    var dataset = [{metric:name, value: value}]

    var texts = svg.selectAll("text")
                .data(dataset)
                .enter();
     
    texts.append("text")
         .text(function(){
              return dataset[0].metric;
         })
         .attr('id', "Name")
         .attr('transform', "translate(" + ((width + margin.left) / 6) + ", " + ((height + margin.top) / 1.5) + ")")
         .attr("font-size",25)
         .style("fill", "#000000");
      

    texts.append("text")
        .text(function(){
            return 0;
        })
        .attr('id', 'scale0')
        .attr('transform', "translate(" + ((width + margin.left) / 100 ) + ", " + ((height + margin.top) / 2) + ")")
        .attr("font-size", 15)
        .style("fill", "#000000");
                    
    texts.append("text")
        .text(function(){
            return gaugeMaxValue/2;
        })
        .attr('id', 'scale10')
        .attr('transform', "translate(" + ((width + margin.left) / 2.15 ) + ", " + ((height + margin.top) / 30) + ")")
        .attr("font-size", 15)
        .style("fill", "#000000");
                    
                    
    texts.append("text")
        .text(function(){
            return gaugeMaxValue;
        })
        .attr('id', 'scale20')
        .attr('transform', "translate(" + ((width + margin.left) / 1.03 ) + ", " + ((height + margin.top) / 2) + ")")
        .attr("font-size", 15)
        .style("fill", "#000000");
    
  var Needle = (function() {

    //Helper function that returns the `d` value for moving the needle
    var recalcPointerPos = function(perc) {
      var centerX, centerY, leftX, leftY, rightX, rightY, thetaRad, topX, topY;
      thetaRad = percToRad(perc / 2);
      centerX = 0;
      centerY = 0;
      topX = centerX - this.len * Math.cos(thetaRad);
      topY = centerY - this.len * Math.sin(thetaRad);
      leftX = centerX - this.radius * Math.cos(thetaRad - Math.PI / 2);
      leftY = centerY - this.radius * Math.sin(thetaRad - Math.PI / 2);
      rightX = centerX - this.radius * Math.cos(thetaRad + Math.PI / 2);
      rightY = centerY - this.radius * Math.sin(thetaRad + Math.PI / 2);
     
        
        return "M " + leftX + " " + leftY + " L " + topX + " " + topY + " L " + rightX + " " + rightY;
        
        
        
        
    };

    function Needle(el) {
      this.el = el;
      this.len = width / 2.5;
      this.radius = this.len / 8;
    }

    Needle.prototype.render = function() {
      this.el.append('circle').attr('class', 'needle-center').attr('cx', 0).attr('cy', 0).attr('r', this.radius);
        
        
        
        
      return this.el.append('path').attr('class', 'needle').attr('id', 'client-needle').attr('d', recalcPointerPos.call(this, 0));
        
       
    };

    Needle.prototype.moveTo = function(perc) {
      var self,
          oldValue = this.perc || 0;

      this.perc = perc;
      self = this;

      // Reset pointer position
      // this.el.transition().delay(100).ease('quad').duration(200).select('.needle').tween('reset-progress', function() {
      //   return function(percentOfPercent) {
      //     var progress = (1 - percentOfPercent) * oldValue;
      //     repaintGauge(progress);
      //     return d3.select(this).attr('d', recalcPointerPos.call(self, progress));
      //   };
      // });

      this.el.transition().delay(300).ease('bounce').duration(1500).select('.needle').tween('progress', function() {
        return function(percentOfPercent) {
          //var progress = percentOfPercent * perc;
          var progress = oldValue + (percentOfPercent * (perc - oldValue));

          
          repaintGauge(progress);
          
          var thetaRad = percToRad(progress / 2);
          var textX = - (self.len + 45) * Math.cos(thetaRad);
          var textY = - (self.len + 45) * Math.sin(thetaRad);

          valueText.text('   '+formatValue(progress)+'   ')          
            .attr('transform', "translate("+textX+","+textY+")")

          return d3.select(this).attr('d', recalcPointerPos.call(self, progress));
        };
      });

    };
    
      
    return Needle;

  })();
    
 
    
  needle = new Needle(chart);
  needle.render();
  needle.moveTo(percent);

  //setTimeout(displayValue, 1350);
    
})();