function selectTitlte(name){
    //console.log(name);
    switch (name) {
        case 'diametro_tubo':
            return "Diametro do tubo";
            break;
        case 'cufomatria':
            return "Cufometria";
            break;
        case 'modo_ventilatorio':
            return "Modo ventilatório";
            break;
        case 'volume_total':
            return "Volume total";
            break;
        case 'volume_minimo':
            return "Volume minuto";
            break;
        case 'freguencia_repiratoria':
            return "Frequência respiratória";
            break;
        case 'tempo_inspiratorio':
            return "Tempo inspiratório";
            break;
        case 'fluxo':
            return "Fluxo";
            break;
        case 'relaxao_ie':
            return "Relação I:E";
            break;
        case 'fio2':
            return "FiO2";
            break;
        case 'pressao_admissao':
            return "Pressão de admissão";
            break;
        case 'pressao_plato':
            return "Pressão de platô";
            break;
        case 'peep':
            return "Peep";
            break;
        case 'auto_peep':
            return "Auto peep";
            break;
        case 'resistencia_respiratoria':
            return "Resistência respiratória";
            break;
        case 'complacencia_estatica':
            return "Complacência estática";
            break;
        case 'complacencia_dinamica':
            return "Complacência dinâmica";
            break;
        case 'ph':
            return "pH";
            break;
        case 'pao2':
            return "PaO2";
            break;
        case 'paco2':
            return "PaCO2";
            break;
        case 'hco3':
            return "HCO3";
            break;
        case 'be':
            return "BE";
            break;
        case 'sato2':
            return "Sat O2";
            break;
        case 'gasometria_p_f':
            return "P / F";
            break;
        case 'anion_gap':
            return "Ânion Gap";
            break;
        case 'ventilometria_freguencia_repiratoria':
            return "Frequência respiratória";
            break;
        case 'ventilometria_volume_minimo':
            return "Volume minuto";
            break;
        case 'volume_corrente':
            return "Volume corrente";
            break;
        case 'indice_tobin':
            return "Índice de tobin (IRRS)";
            break;
        case 'driving_pressure':
            return "Driving pressure (Distensão)";
            break;
        case 'pressao_inspiratoria_maxima':
            return "Pressão inspiratória Máxima";
            break;
        case 'pressao_expiratoria_maxima':
            return "Pressão expiratória máxima";
            break;
        case 'etco2':
            return "EtCO2";
            break;
        case 'oximetria':
            return "Oximetria";
            break;
            
        case 'conteudo_arterial_o2':
            return "Conteúdo arterial de O2";
            break;
        case 'escala_rass':
            return "Escala de RASS";
            break;
        case 'escala_cpot':
            return "Escala de CPOT";
            break;
        case 'escala_glasgow':
            return "Escala de Glasgow";
            break;
        case 'uso_sedativo':
            return "Uso de sedativo";
            break;
        case 'uso_analgesicos':
            return "Uso de analgésicos";
            break;
        case 'uso_bnm':
            return "Uso de BNM";
            break;
        case 'secrecao_traqueal':
            return "Secreção traqueal";
            break;
        default: 
            return "Ainda não terminado";
            break;
    }
}