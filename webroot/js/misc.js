/*!
 *	Gerador e Validador de CPF v1.0.0
 *	https://github.com/tiagoporto/gerador-validador-cpf
 *	Copyright (c) 2014-2015 Tiago Porto (http://www.tiagoporto.com)
 *	Released under the MIT license
 */
function CPF()
{
	"user_strict";

	function r( r )
	{
		for ( var t = null, n = 0; 9 > n; ++n ) t += r.toString().charAt( n ) * ( 10 - n );
		var i = t % 11;
		return i = 2 > i ? 0 : 11 - i
	}

	function t( r )
	{
		for ( var t = null, n = 0; 10 > n; ++n ) t += r.toString().charAt( n ) * ( 11 - n );
		var i = t % 11;
		return i = 2 > i ? 0 : 11 - i
	}
	var n = "CPF Inválido",
		i = "CPF Válido";
	this.gera = function ()
	{
		for ( var n = "", i = 0; 9 > i; ++i ) n += Math.floor( 9 * Math.random() ) + "";
		var o = r( n ),
			a = n + "-" + o + t( n + "" + o );
		return a
	}, this.valida = function ( o )
	{
		for ( var a = o.replace( /\D/g, "" ), u = a.substring( 0, 9 ), f = a.substring( 9, 11 ), v = 0; 10 > v; v++ )
			if ( "" + u + f == "" + v + v + v + v + v + v + v + v + v + v + v ) return n;
		var c = r( u ),
			e = t( u + "" + c );
		return f.toString() === c.toString() + e.toString() ? i : n
	}
}
var CPF = new CPF();
// document.write(CPF.valida("123.456.789-00"));
// document.write("<br> Utilizando o proprio gerador da lib<br><br><br>");
// for(var i =0;i<40;i++) {
//    var temp_cpf = CPF.gera();
//    document.write(temp_cpf+" = "+CPF.valida(temp_cpf)+"<br>");
// }
$( "#cpf" ).keypress( function ()
{
	$( "#cpf_status" ).html( CPF.valida( $( this ).val() ) );
	console.log( CPF.valida( $( this ).val() ) );
} );
$( "#cpf" ).blur( function ()
{
	$( "#cpf_status" ).html( CPF.valida( $( this ).val() ) );
	console.log( CPF.valida( $( this ).val() ) );
	resetCpf();
} );
$( "#document-number" ).keypress( function ()
{
	$( "#cpf_status" ).html( CPF.valida( $( this ).val() ) );
	console.log( CPF.valida( $( this ).val() ) );
} );
$( "#document-number" ).blur( function ()
{
	$( "#cpf_status" ).html( CPF.valida( $( this ).val() ) );
	console.log( CPF.valida( $( this ).val() ) );
	resetCpf();
} );

function minutesToStr( minutes )
{
	var sign = '';
	if ( minutes < 0 )
	{
		sign = '-';
	}
	var hours = leftPad( Math.floor( Math.abs( minutes ) / 60 ) );
	//var minutes = leftPad( (Math.abs(minutes) % 60).toFixed(2) );  
	var minutes = leftPad( Math.round( Math.abs( minutes ) % 60 ) );
	return sign + hours + 'h ' + minutes + 'min';
}

function leftPad( number )
{
	return ( ( number < 10 && number >= 0 ) ? '0' : '' ) + number;
}

function inArray( needle, haystack )
{
	var length = haystack.length;
	for ( var i = 0; i < length; i++ )
	{
		if ( haystack[ i ] == needle ) return true;
	}
	return false;
}

function resetCpf()
{
	// console.log($("#cpf_status").text());
	if ( $( "#cpf_status" ).text() == "CPF Inválido" )
	{
		$( "#cpf" ).val( "" );
		$( "#document-number" ).val( "" );
	}
}
$( document ).ready( function ()
{
	$( window ).resize( function ()
	{
		ellipses1 = $( "#breadcumb :nth-child(2)" );
		if ( $( "#breadcumb a:hidden" ).length > 0 )
		{
			ellipses1.show();
		}
		else
		{
			ellipses1.hide();
		}
	} )
} );
var _validFileExtensions = [ ".jpg", ".jpeg", ".png" ];

function Validate( oForm )
{
	var arrInputs = oForm.getElementsByTagName( "input" );
	for ( var i = 0; i < arrInputs.length; i++ )
	{
		var oInput = arrInputs[ i ];
		if ( oInput.type == "file" )
		{
			var sFileName = oInput.value;
			if ( sFileName.length > 0 )
			{
				var blnValid = false;
				for ( var j = 0; j < _validFileExtensions.length; j++ )
				{
					var sCurExtension = _validFileExtensions[ j ];
					if ( sFileName.substr( sFileName.length - sCurExtension.length, sCurExtension.length ).toLowerCase() == sCurExtension.toLowerCase() )
					{
						blnValid = true;
						break;
					}
				}
				if ( !blnValid )
				{
					// alert("O arquivo " + sFileName + " é Inválido, as extensões permitidas são: " + _validFileExtensions.join(", "));
					$.alert(
					{
						title: 'Extensão Inválida!',
						content: 'As extensões permitidas são: jpeg, jpg e png.',
					} );
					return false;
				}
			}
		}
	}
	return true;
}
var _URL = window.URL;
$( ".validate-img" ).change( function ( e )
{
	console.log( "validate-img" );
	var file, img;
	var currentObj = this;
	if ( ( file = this.files[ 0 ] ) )
	{
		// console.log(this.files[0]);
		img = new Image();
		img.onload = function ()
		{
			// alert("Width:" + this.width + "   Height: " + this.height);//this will give you image width and height and you can easily validate here....
			if ( this.width > 130 || this.width < 65 )
			{
				$.alert(
				{
					title: 'Erro!',
					content: 'A imagem não está no tamanho correto. O tamanho recomendado é 120px de largura e 40px de altura.',
				} );
				$( "#group-submit" ).prop( "disabled", true );
				resetInputFile();
			}
			else if ( this.height > 60 || this.height < 30 )
			{
				$.alert(
				{
					title: 'Erro!',
					content: 'A imagem não está no tamanho correto. O tamanho recomendado é 120px de largura por 40px de altura.',
				} );
				$( "#group-submit" ).prop( "disabled", true );
				resetInputFile();
			}
			// else if ( ( this.width + this.height ) > 827 )
			// {
			// 	$.alert(
			// 	{
			// 		title: 'Erro!',
			// 		content: 'A imagem fornecida é muito grande. O tamanho recomendado é 120px de largura por 40px de altura.',
			// 	} );
			// 	$( "#group-submit" ).prop( "disabled", true );
			// 	resetInputFile();
			// }
			// else if ( this.width <= this.height )
			// {
			// 	$.alert(
			// 	{
			// 		title: 'Erro!',
			// 		content: 'A imagem deve estar no formato de paisagem.',
			// 	} );
			// 	$( "#group-submit" ).prop( "disabled", true );
			// 	resetInputFile();
			// }
			else
			{
				// console.log("else");
				readURL( currentObj );
				$( "#group-submit" ).prop( "disabled", false );
			}
		};
		img.src = _URL.createObjectURL( file );
	}
} );

function resetInputFile()
{
	console.log( "resetInputFile INIT" );
	$( ".validate-img" ).replaceWith( $( ".validate-img" ).val( '' ).clone( true ) );
	$( '.img-thumbnail' ).prop( 'src', '/gh-backoffice/img/Logo-GH.png' );
}

function readURL( input )
{
	console.log( "readURL INIT" );
	if ( input.files && input.files[ 0 ] )
	{
		var reader = new FileReader();
		reader.onload = function ( e )
		{
			$( '.img-thumbnail' ).attr( 'src', e.target.result );
		}
		reader.readAsDataURL( input.files[ 0 ] );
	}
}

function toggleCheckBox( id )
{
	var label = "label[for=" + id + "]"; //label selector 
	$( label ).toggle();
	if ( $( '#' + id ).is( ':checked' ) )
	{
		$( '#' + id ).prop( 'checked', false ); // Unchecks it
	}
}

function toggleElement( elem )
{
	console.log( 'toggle' );
	$( elem ).toggle();
	$( elem ).val( '' ); //clear input text
}

function getAge( dateString )
{
	if ( dateString !== null && dateString !== 'undefined' )
	{
		var today = new Date();
		var birthDate = new Date( dateString );
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if ( m < 0 || ( m === 0 && today.getDate() < birthDate.getDate() ) )
		{
			age--;
		}
		return age;
	}
	return 0;
}

function disableClick()
{
	$( '.funkyradio-primary' ).each( function ( index )
	{
		$( this ).css( "pointer-events", "none" );
	} );
}
( function ( $ )
{
	$.fn.numeric = function ( options )
	{
		return this.each( function ()
		{
			var $this = $( this );
			$this.keypress( options, function ( e )
			{
				// allow backspace and delete 
				if ( e.which == 8 || e.which == 0 ) return true;
				//if the letter is not digit 
				if ( e.which < 48 || e.which > 57 ) return false;
				// check max range 
				var dest = e.which - 48;
				var result = this.value + dest.toString();
				if ( result > e.data.max )
				{
					return false;
				}
			} );
		} );
	};
} )( jQuery );
$( '.int-limit-3' ).numeric(
{
	max: 999
} );