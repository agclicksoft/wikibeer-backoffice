$( document ).ready( function ()
{
    $( ".active_checked" ).change( function ()
    {
        var id = $( this ).attr( "id" );
        var is_checked = false;
        //console.log(" id = " + id);
        // console.log("select = "+$("#"+ id).is("select"));
        // console.log("checkbox = "+$("#"+ id).is("input[type='checkbox']"));
        if ( $( "#" + id ).is( "input[type='checkbox']" ) )
        {
            if ( $( "#" + id ).is( ':checked' ) )
            {
                $( "." + id ).slideDown();
                is_checked = true;
            }
        }
        else if ( $( "#" + id ).is( "select" ) )
        {
            if ( $( "#" + id ).val() !== "" )
            {
                $( "." + id ).slideDown();
                is_checked = true;
            }
        }
        if ( !is_checked )
        {
            $( "." + id ).slideUp();
            //clear sub checks
            $( "." + id + ' .active_checked' ).trigger( "change" );
            //clear select
            $( "." + id + " select option:selected" ).prop( "selected", false )
                //clear input
            $( "." + id + " input[type='text']" ).val( '' );
            var id2 = $( "." + id + " input[type='number']" ).val( '' );
            var id2 = $( "." + id + " input[type='date']" ).val( '' );
            var id2 = $( "." + id + " input[type='time']" ).val( '' );
            var id2 = $( "." + id + " textarea" ).val( '' );
            //clear checkbox
            $( "." + id + ' *' ).prop( 'checked', false );
            var id2 = $( "." + id + " input[type='checkbox']" ).attr( 'id' );
            //console.log(" id2 = " + id2);
            if ( !$( "#" + id2 ).is( ':checked' ) )
            {
                //$( "." + id2 ).slideUp();
                $( "." + id ).slideUp();
            }
        }
    } );
    
    $( ".active_checked_2" ).change( function ()
    {
        var id = $( this ).attr( "id" );
        var is_checked = true;
        if ( $( "#" + id ).is( "input[type='checkbox']" ) )
        {
            if ( $( "#" + id ).is( ':checked' ) )
            {
                $( "." + id ).slideUp();
                is_checked = false;
                //clear input
                var id2 = $( "." + id + " input[type='text']" ).val( '' );
                var id2 = $( "." + id + " textarea" ).val( '' );
            }
        }
        if ( is_checked )
        {
            $( "." + id ).slideDown();
        }
    } );
    $( ".active_checked" ).trigger( "change" );
    $( ".active_checked_2" ).trigger( "change" );
    $( '#olvidado' ).click( function ( e )
    {
        e.preventDefault();
        $( 'div#form-olvidado1' ).toggle( '500' );
    } );
    $( '#olvidado' ).click( function ( e )
    {
        e.preventDefault();
        $( 'div#form-olvidado' ).toggle( '500' );
    } );
    $( '#acceso1' ).click( function ( e )
    {
        e.preventDefault();
        $( 'div#form-olvidado' ).toggle( '500' );
    } );
    $( '#acceso1' ).click( function ( e )
    {
        e.preventDefault();
        $( 'div#form-olvidado1' ).toggle( '500' );
    } );
} );
$( document ).ready( function ()
{
    $( '#olvidado2' ).click( function ( e )
    {
        e.preventDefault();
        $( 'div#form-olvidado2' ).toggle( '500' );
    } );
    $( '#olvidado2' ).click( function ( e )
    {
        e.preventDefault();
        $( 'div#form-olvidado' ).toggle( '500' );
    } );
    $( '#acceso2' ).click( function ( e )
    {
        e.preventDefault();
        $( 'div#form-olvidado' ).toggle( '500' );
    } );
    $( '#acceso2' ).click( function ( e )
    {
        e.preventDefault();
        $( 'div#form-olvidado2' ).toggle( '500' );
    } );
} );
$( document ).ready( function ()
{
    if ( $( '.multiselect' ).length )
    {
        $( '.multiselect' ).multiselect(
        {
            maxHeight: 200,
            filterBehavior: 'value'
        } );
    }
    if ( $( '#auxiliary-specialties-ids' ).length )
    {
        $( '#auxiliary-specialties-ids' ).multiselect(
        {
            maxHeight: 200,
            filterBehavior: 'value'
        } );
    }
    if ( $( '#change-specialties-ids' ).length )
    {
        $( '#change-specialties-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true
        } );
    }
    if ( $( '#route-of-anesthesias-ids' ).length )
    {
        $( '#route-of-anesthesias-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true
        } );
    }
    if ( $( '#professionals-0-sectors-ids' ).length )
    {
        $( '#professionals-0-sectors-ids' ).multiselect(
        {
            maxHeight: 200,
            filterBehavior: 'value'
        } );
    }
    if ( $( '#risk-factors-ids' ).length )
    {
        $( '#risk-factors-ids' ).multiselect(
        {
            maxHeight: 200,
            filterBehavior: 'value'
        } );
    }
    if ( $( '#sectors-ids' ).length )
    {
        $( '#sectors-ids' ).multiselect(
        {
            maxHeight: 200,
            filterBehavior: 'value'
        } );
    }
    if ( $( '#tev-options-ids' ).length )
    {
        $( '#tev-options-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true,
            filterBehavior: 'value'
        } );
    }
    if ( $( '#pain-characteristics-ids' ).length )
    {
        $( '#pain-characteristics-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true
                //filterBehavior: 'value'
        } );
    }
    if ( $( '#pain-characteristics-ids' ).length )
    {
        $( '#pain-characteristic-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true
                //filterBehavior: 'value'
        } );
    }
    if ( $( '#pain-characteristics-ids' ).length )
    {
        $( '#pain-characteristics-id' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true
                //filterBehavior: 'value'
        } );
    }
    if ( $( '#list-antibiotics-ids' ).length )
    {
        $( '#list-antibiotics-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true,
            // filterBehavior: 'value'
        } );
    }
    if ( $( '#professionals-0-specialties-ids' ).length )
    {
        $( '#professionals-0-specialties-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true
        } );
    }
    if ( $( '#specialties-ids' ).length )
    {
        $( '#specialties-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true
        } );
    }
    if ( $( '#areas-of-occupation' ).length )
    {
        $( '#areas-of-occupation' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#comorbidities-ids' ).length )
    {
        $( '#comorbidities-ids' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#other-comorbidities-ids' ).length )
    {
        $( '#other-comorbidities-ids' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#defects-and-anomalies-ids' ).length )
    {
        $( '#defects-and-anomalies-ids' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '.multiselect_yo' ).length )
    {
        $( '.multiselect_yo' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#type-of-anesthesias-ids' ).length )
    {
        $( '#type-of-anesthesias-ids' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#professionals-ids' ).length )
    {
        $( '#professionals-ids' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#professionals-ids' ).length )
    {
        $( '#professionals-ids' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#immediate-complications-ids' ).length )
    {
        $( '#immediate-complications-ids' ).multiselect(
        {
            maxHeight: 200,
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true
        } );
    }
    if ( $( '#surgical-complications-ids' ).length )
    {
        $( '#surgical-complications-ids' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#type_of_anesthesias_idd' ).length )
    {
        $( '#type_of_anesthesias_idd' ).multiselect(
        {
            maxHeight: 200
        } );
    }
    if ( $( '#potenciais-orgaos-ids' ).length )
    {
        $( '#potenciais-orgaos-ids' ).multiselect(
        {
            maxHeight: 200,
        } );
    }
    if ( $( '#orgaos-captados-ids' ).length )
    {
        $( '#orgaos-captados-ids' ).multiselect(
        {
            maxHeight: 200,
        } );
    }

    $( '.mes-ano' ).datepicker(
    {
        format: "mm/yyyy",
        language: "pt-BR",
        multidate: false
    } );
    
    if ( $( "title" ).text() !== "Login" )
    {
        $( '#birthday' ).datepicker(
        {
            format: "dd/mm/yyyy",
            language: "pt-BR",
            multidate: false
        } );
        $( '#expected-discharge-day' ).datepicker(
        {
            format: "dd/mm/yyyy",
            language: "pt-BR",
            multidate: false
        } );
        $( '#diagnostic-date' ).datepicker(
        {
            format: "dd/mm/yyyy",
            language: "pt-BR",
            multidate: false
        } );
        $( '.date' ).datepicker(
        {
            format: "dd/mm/yyyy",
            language: "pt-BR",
            multidate: false
        } );
    }
    $( "#goback" ).click( function ()
    {
        goBack();
    } );
} );

function goBack()
{
    window.history.back();
}
jQuery( function ( $ )
{
    //var htmlHeight = $( document ).height();
    //$('html').height(htmlHeight);
    //$('html').style.minHeight = htmlHeight;
    if ( $( "title" ).text() !== "Login" )
    {
        $( "#birthday" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#diagnostic-date" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#expected-discharge-day" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#date-instalation" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#date-of-withdrawal" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#reinsert-date" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#data-inicio" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#data-termino" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#data-da-instalacao" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( "#data-da-retirada" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        $( ".date" ).mask( "99/99/9999",
        {
            placeholder: "dd/mm/yyyy"
        } );
        //$.mask.definitions['~'] = '([0-9] )?';
        //$("#phone-number").mask("(99) ?99999-9999")
        $( "#cellphone-number" ).mask( "(99) 9999-9999?9" ).focusout( function ( event )
        {
            var target, phone, element;
            target = ( event.currentTarget ) ? event.currentTarget : event.srcElement;
            phone = target.value.replace( /\D/g, '' );
            element = $( target );
            element.unmask();
            if ( phone.length > 10 )
            {
                element.mask( "(99) 99999-999?9" );
            }
            else
            {
                element.mask( "(99) 9999-9999?9" );
            }
        } );
        $( "#phone-number" ).mask( "(99) 9999-9999?9" ).focusout( function ( event )
        {
            var target, phone, element;
            target = ( event.currentTarget ) ? event.currentTarget : event.srcElement;
            phone = target.value.replace( /\D/g, '' );
            element = $( target );
            element.unmask();
            if ( phone.length > 10 )
            {
                element.mask( "(99) 99999-999?9" );
            }
            else
            {
                element.mask( "(99) 9999-9999?9" );
            }
        } );
        $( "#telephone-number" ).mask( "(99) 9999-9999?9" ).focusout( function ( event )
        {
            var target, phone, element;
            target = ( event.currentTarget ) ? event.currentTarget : event.srcElement;
            phone = target.value.replace( /\D/g, '' );
            element = $( target );
            element.unmask();
            if ( phone.length > 10 )
            {
                element.mask( "(99) 99999-999?9" );
            }
            else
            {
                element.mask( "(99) 9999-9999?9" );
            }
        } );
        $( ".telephone-number" ).mask( "(99) 9999-9999?9" ).focusout( function ( event )
        {
            var target, phone, element;
            target = ( event.currentTarget ) ? event.currentTarget : event.srcElement;
            phone = target.value.replace( /\D/g, '' );
            element = $( target );
            element.unmask();
            if ( phone.length > 10 )
            {
                element.mask( "(99) 99999-999?9" );
            }
            else
            {
                element.mask( "(99) 9999-9999?9" );
            }
        } );
        $( "#telephone" ).mask( "(99) 9999-9999" );
        $( "#telephones-ids" ).mask( "(99) 9999-9999" );
        $( "#professionals-0-telephone" ).mask( "(99) 9999-9999" );
        $( "#cep" ).mask( "99999-999" );
        $( ".cep" ).mask( "99999-999" );
        $( '#cpf' ).mask( '999.999.999-99' );
        $( '#document-number' ).mask( '999.999.999-99' );
        $( '#professionals-0-cpf' ).mask( '999999999-99' );
        $( '.time' ).mask( '99:99' );
        $( '.temp' ).mask( '99,9' );
        $( '#hour-instalation' ).mask( '99:99' );
        $( '#withdrawal-time' ).mask( '99:99' );
        $( '#reinsert-time' ).mask( '99:99' );
        $( '#hora-da-instalacao' ).mask( '99:99' );
        $( '#hora-da-retirada' ).mask( '99:99' );
    }
    $( ".collapseAccordion" ).hover( function ()
    {
        //$(this).trigger('click');
    } );
} );