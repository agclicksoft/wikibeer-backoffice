-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: beerbizz_db
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barcodes`
--

DROP TABLE IF EXISTS `barcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) DEFAULT NULL,
  `beer_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_table1_beers1_idx` (`beer_id`),
  CONSTRAINT `fk_table1_beers1` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barcodes`
--

LOCK TABLES `barcodes` WRITE;
/*!40000 ALTER TABLE `barcodes` DISABLE KEYS */;
INSERT INTO `barcodes` VALUES (1,'0000000000',6,NULL),(2,'1211111',7,NULL),(3,'666666',7,NULL),(4,'333333',7,NULL),(6,'',9,NULL),(7,'',10,NULL),(8,'',11,NULL),(16,'8889990',8,NULL),(23,'7898578680242',16,NULL),(24,'9898989898',16,NULL),(25,'7898578680013',17,NULL),(26,'00000123',17,NULL),(27,'7898578680013',18,NULL),(30,'7898578680013',22,NULL),(31,'234234',22,NULL),(32,'666666',22,NULL),(33,'7898578680013',23,NULL),(34,'234234',23,NULL),(35,'666666',23,NULL);
/*!40000 ALTER TABLE `barcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beers`
--

DROP TABLE IF EXISTS `beers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beer_name` varchar(255) DEFAULT NULL,
  `manufacturer_name` varchar(255) DEFAULT NULL,
  `alcohol_content` varchar(255) DEFAULT '',
  `bitterness` varchar(255) DEFAULT NULL,
  `harmonization` text,
  `style` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `colour` varchar(255) DEFAULT NULL,
  `glass_type` varchar(255) DEFAULT NULL,
  `calories` float DEFAULT NULL,
  `ideal_temperature` varchar(255) DEFAULT '',
  `image` varchar(255) DEFAULT NULL,
  `average_price` float DEFAULT NULL,
  `comercial_presentation` mediumtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `packing` varchar(255) DEFAULT NULL,
  `seasonality` varchar(255) DEFAULT NULL,
  `history` varchar(255) DEFAULT NULL,
  `brewery_partner_group` varchar(255) DEFAULT NULL,
  `evaluations_total` int(11) DEFAULT '0',
  `visits` int(11) DEFAULT '0',
  `importer` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beers`
--

LOCK TABLES `beers` WRITE;
/*!40000 ALTER TABLE `beers` DISABLE KEYS */;
INSERT INTO `beers` VALUES (6,'Brahma','12','12','','asa','asas','12','12','12','12','12',12,'12','',12.99,'Ótima','2017-03-16 20:32:41','2017-03-17 19:14:14',1,'sadsad','asdas','asd','asd',NULL,0,''),(7,'bhraama','21212','1212','22','sad','asds','asd','asd','asd','asd','as',12,NULL,'021b2920-4d14-405a-b62c-d780f3d2b773-caveira.png',12,'zxcxzc','2017-03-16 20:35:35','2017-03-17 13:42:49',1,'asd','asd','asd','asd',NULL,0,''),(8,'ffffff','',NULL,'','qwe','qwewqe','qwewqe','qweqe','eqwewq','','',12,NULL,'',NULL,'','2017-03-17 12:29:04','2017-03-17 18:50:02',1,'qwe','','','',NULL,0,''),(9,'sdfsdfdsf','',NULL,'','','','','','','','',NULL,NULL,'1cbbb7c1-f136-4e8a-bf14-589e35f8c48a-skull.png',NULL,'','2017-03-17 12:52:36','2017-03-17 13:41:40',0,'','','','',0,0,''),(10,'dafgshdgjfkhhl','',NULL,'','','','asas','asas','asas','asas','',NULL,NULL,'',NULL,'','2017-03-17 12:57:37','2017-03-17 12:58:58',0,'','','','',0,0,''),(11,'asdsadsadsad','',NULL,'','','','','','','','',NULL,NULL,'',NULL,'','2017-03-17 12:58:08','2017-03-17 12:58:53',0,'','','','',0,0,''),(16,'Bodebrown Cacau IPA ','Bodebrown','6,1','NI','Abacate (Guacamole ou Salada), Carne Assada, Carne com Chili, Chouriço, Enchiladas (Tortilhas), Empanadas, Filet Mignon au poivre, Grão de Bico (bolinhos/ Falafel), Hamburger, Nachos, Pato au Poivre, Pimentão Recheado, Pizza Calabresa, Salmão, Queijo Grana Padano','Indian Pale Ale','Brasil','PR','Curitiba','Cobre','Pint/Caldereta',183,'5-7`C','',NULL,'Uma parceria entre a Bodebrown e a cervejaria norte-americana Stone Brewing, a Cacau IPA é um cerveja premiada como a melhor do estilo no Brasil em 2013 no evento IPA Day. Ela une com maestria características da India Pale Ale tradicional inglesa com toques brasileiros do cacau em formato “nibs” durante fervura e maturação. De aromas marcantes, tem amargor médio e notas de caramelo com lúpulos cítricos em uma bebida de 6,1% de grau alcoólico. Foi a primeira cerveja IPA brasileira a contar com adição de cacau.','2017-03-19 22:33:42','2017-03-19 22:33:42',1,'Grfa 300','Serie','','',95,0,''),(17,'Bodebrown Wee Heavy ','Bodebrown','8','20','Cogumelos, Cordeiro Assado, Foie Gras, Javali, Ossobuco, Panna Cotta, Rabada, Veado','Strong Scotch Ale','Brasil','PR','Curitiba','Cobre','Taça',240,'8-12`C','',NULL,'Medalha de ouro no Mondial de La Bière de 2011 e 2013 no Canadá Medalha de prata no Festival da Cerveja de Blumenal em 2013 e 2014, Medalha de Prata no Australian International Beer Awards em 2012 e Medalha de Bronze no Australian International Beer Awards em 2013 a Wee Heavy é uma clássica Scotch Ale. Encorpada, conta com grau alcoólico de 8% e amargor médio/baixo. Possui certa doçura do malte evidente e apresente aromas maltados, tostados, turfados e caramelados, com uma tonalidade marrom-clara. Sua receita é inspirada na tradição dos monges beneditinos em terras escocesas do início do século XVIII.','2017-03-19 22:33:42','2017-03-19 22:33:42',1,'Grfa 500 ','Serie ','','',97,0,''),(18,'Skoll Beats Vermelha','Bodebrown','8','20','Cogumelos, Cordeiro Assado, Foie Gras, Javali, Ossobuco, Panna Cotta, Rabada, Veado','Strong Scotch Ale','Brasil','PR','Curitiba','Cobre','Taça',240,'8-12`C','',NULL,'Medalha de ouro no Mondial de La Bière de 2011 e 2013 no Canadá Medalha de prata no Festival da Cerveja de Blumenal em 2013 e 2014, Medalha de Prata no Australian International Beer Awards em 2012 e Medalha de Bronze no Australian International Beer Awards em 2013 a Wee Heavy é uma clássica Scotch Ale. Encorpada, conta com grau alcoólico de 8% e amargor médio/baixo. Possui certa doçura do malte evidente e apresente aromas maltados, tostados, turfados e caramelados, com uma tonalidade marrom-clara. Sua receita é inspirada na tradição dos monges beneditinos em terras escocesas do início do século XVIII.','2017-03-20 00:02:28','2017-03-20 00:02:28',1,'Grfa 500 ','Serie ','','',97,0,''),(21,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'2017-03-20 04:23:09','2017-03-20 04:23:09',1,NULL,NULL,NULL,NULL,0,0,''),(22,'Skoll Beats Verde','Bodebrown','8','20','Cogumelos, Cordeiro Assado, Foie Gras, Javali, Ossobuco, Panna Cotta, Rabada, Veado','Strong Scotch Ale','Brasil','PR','Curitiba','Cobre','Taça',240,'8-12`C','',NULL,'Medalha de ouro no Mondial de La Bière de 2011 e 2013 no Canadá Medalha de prata no Festival da Cerveja de Blumenal em 2013 e 2014, Medalha de Prata no Australian International Beer Awards em 2012 e Medalha de Bronze no Australian International Beer Awards em 2013 a Wee Heavy é uma clássica Scotch Ale. Encorpada, conta com grau alcoólico de 8% e amargor médio/baixo. Possui certa doçura do malte evidente e apresente aromas maltados, tostados, turfados e caramelados, com uma tonalidade marrom-clara. Sua receita é inspirada na tradição dos monges beneditinos em terras escocesas do início do século XVIII.','2017-03-20 04:25:08','2017-03-20 04:25:08',1,'Grfa 500 ','Serie ','','',97,0,''),(23,'asodjoiasdjoiasjdoij','Bodebrown','8','20','Cogumelos, Cordeiro Assado, Foie Gras, Javali, Ossobuco, Panna Cotta, Rabada, Veado','Strong Scotch Ale','Brasil','PR','Curitiba','Cobre','Taça',240,'8-12`C','',NULL,'Medalha de ouro no Mondial de La Bière de 2011 e 2013 no Canadá Medalha de prata no Festival da Cerveja de Blumenal em 2013 e 2014, Medalha de Prata no Australian International Beer Awards em 2012 e Medalha de Bronze no Australian International Beer Awards em 2013 a Wee Heavy é uma clássica Scotch Ale. Encorpada, conta com grau alcoólico de 8% e amargor médio/baixo. Possui certa doçura do malte evidente e apresente aromas maltados, tostados, turfados e caramelados, com uma tonalidade marrom-clara. Sua receita é inspirada na tradição dos monges beneditinos em terras escocesas do início do século XVIII.','2017-03-20 04:33:02','2017-03-20 04:33:02',1,'Grfa 500 ','Serie ','','',97,0,'');
/*!40000 ALTER TABLE `beers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phinxlog`
--

DROP TABLE IF EXISTS `phinxlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phinxlog`
--

LOCK TABLES `phinxlog` WRITE;
/*!40000 ALTER TABLE `phinxlog` DISABLE KEYS */;
INSERT INTO `phinxlog` VALUES (20170316152748,'BeersUpdate','2017-03-16 19:06:17','2017-03-16 19:06:18',0),(20170316161011,'BeersUpdate2','2017-03-16 19:19:08','2017-03-16 19:19:08',0),(20170316162049,'BeersUpdate3','2017-03-16 19:23:04','2017-03-16 19:23:04',0),(20170316162423,'BeersUpdate4','2017-03-16 19:24:33','2017-03-16 19:24:33',0),(20170317124500,'AddVisits','2017-03-17 15:46:19','2017-03-17 15:46:20',0),(20170317124931,'ChangeEvaluations','2017-03-17 15:50:48','2017-03-17 15:50:48',0),(20170317204953,'UpdateFieldsSuggestions','2017-03-17 23:53:52','2017-03-17 23:53:52',0),(20170317205713,'UpdateFieldsSuggestions1','2017-03-17 23:59:04','2017-03-17 23:59:04',0),(20170319192607,'UpdateTable','2017-03-19 22:31:54','2017-03-19 22:31:55',0),(20170319194408,'UpdateTableHarmonization','2017-03-19 22:49:03','2017-03-19 22:49:03',0);
/*!40000 ALTER TABLE `phinxlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_score` float DEFAULT NULL,
  `beer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ratings_beers_idx` (`beer_id`),
  CONSTRAINT `fk_ratings_beers` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (5,5,6),(10,3.9,16),(11,4,17),(12,4,18),(15,4,22),(16,4,23);
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suggestions`
--

DROP TABLE IF EXISTS `suggestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suggestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beer_name` varchar(255) DEFAULT NULL,
  `suggestions_total` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suggestions`
--

LOCK TABLES `suggestions` WRITE;
/*!40000 ALTER TABLE `suggestions` DISABLE KEYS */;
INSERT INTO `suggestions` VALUES (3,'Skoll 2',3,'2017-03-17 21:55:46'),(4,'Skoll 9',NULL,'2017-03-17 22:03:16'),(5,'Skoll 09',2,'2017-03-17 22:04:16');
/*!40000 ALTER TABLE `suggestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Rainyer Santiago','rainyer.santiago@clicksoft.com.br','$2y$10$gFPKImgYblL3v7AF5yemX.WMH24GZL5Cy2H5FefGS14w7jYqqLPAu','2017-03-15 17:18:43','2017-03-15 17:18:43',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-20 12:06:00
