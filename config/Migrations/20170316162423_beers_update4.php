<?php

use Phinx\Migration\AbstractMigration;

class BeersUpdate4 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('beers');
        $table->changeColumn('packing', 'string',[
                'default' => null,
                'null' => true,
            ])
            ->changeColumn('seasonality', 'string',[
                'default' => null,
                'null' => true,
            ])
            ->changeColumn('history', 'string',[
                'default' => null,
                'null' => true,
            ])
            ->changeColumn('brewery_partner_group', 'string',[
                'default' => null,
                'null' => true,
            ])
            ->changeColumn('evaluations_total', 'integer',[
                'default' => null,
                'null' => true,
            ])

            ->update();
    }
}
