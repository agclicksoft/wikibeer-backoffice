<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= ($title != '' && $title != null) ? $title : "BeerSpy" ?></title>
    <link rel="icon" href="http://199.201.88.171/~beerbizz/beerbizz/img/logo_beer.png" type="image/gif" sizes="16x16">


    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<?= $this->Html->script('jquery.min.js');?>
    <?=  $this->Html->css('custom'); ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?=  $this->Html->css('custom'); ?>
    <?=  $this->Html->css('normalize'); ?>
    <?=  $this->Html->css('component'); ?>
    <?=  $this->Html->css('bootstrap-multiselect'); ?>
    <?=  $this->Html->css('bootstrap-datepicker.min'); ?>
    <?=  $this->Html->css('jquery-confirm.min'); ?>
    <?=  $this->Html->css('jquery-ui'); ?>
    <?=  $this->Html->css('daterangepicker'); ?>
    <?=  $this->Html->css('bootstrap-datetimepicker.min'); ?>
    <?= $this->Html->css('font-awesome.min') ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <!-- <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/beers/index">
	      	<?= $this->Html->image('ribbon.png', ['alt' => 'Beer']); ?></a>
	    </div> -->

        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a id='brand-link' class="navbar-brand" href="/~beerbizz/beerbizz/beers/index">

          </a>

        </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <?php
	      	$default_nav_bar_left = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'nav-bar-left.ctp';
	      	if (file_exists($default_nav_bar_left)) {
	      		ob_start();
	      		include $default_nav_bar_left;
	      		echo ob_get_clean();
	      	}
	      	else {
	      		echo $this->element('nav-bar-left');
	      	}
      	  ?>
	      <?php
	      	$default_nav_bar_right = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'nav-bar-right.ctp';
	      	if (file_exists($default_nav_bar_right)) {
	      		ob_start();
	      		include $default_nav_bar_right;
	      		echo ob_get_clean();
	      	}
	      	else {
	      		echo $this->element('nav-bar-right');
	      	}
      	  ?>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container -->
	</nav>
	<section class="container clearfix">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </section>

    <?php
	  	$default_footer = ROOT.DS.'src'.DS.'Template'.DS.'Element'.DS.'footer.ctp';
	  	if (file_exists($default_footer)) {
	  		ob_start();
	  		include $default_footer;
	  		echo ob_get_clean();
	  	}
	  	else {
	  		echo $this->element('footer');
	  	}
	?>

    <?php
        
	    echo $this->Html->script('jquery.maskedinput.min');
      echo $this->Html->script('jquery.maskMoney');
	    echo $this->Html->script('jquery-ui.min');
	    echo $this->Html->script('jquery-confirm.min');
      echo $this->Html->script('jquery.ddslick.min');

      echo $this->Html->script('bootstrap.min.js');
      echo $this->Html->script('bootstrap-multiselect');
  	  echo $this->Html->script('bootstrap-datepicker.min');
  	  echo $this->Html->script('bootstrap-datepicker.pt-BR.min');
      echo $this->Html->script('validator.min.js');

      echo $this->Html->script('loadingoverlay');
      echo $this->Html->script('login');
      echo $this->Html->script('misc');
      echo $this->Html->script('moment.min');
      echo $this->Html->script('daterangepicker');
      echo $this->Html->script('bootstrap-datetimepicker.min');
      echo $this->Html->script('bootstrap-datetimepicker.pt-BR');
      
    ?>

    <?= $this->fetch('script') ?>

</body>
</html>
